# hho_expfit

## README

### Description

The code hho_expfitt is an implementation of mixed HHO schemes for linear advection-diffusion equation.
In order to handle noncoercive problems, it is based on the exponential-fitting strategy, which enables one to obtain inconditionnal coercivity for any irrotational advection field.

This code was developped in order to investigate the long-time behaviour as well as the lack of positivity of linear high-order schemes on simple problems.
It compute a numerical approximation of a transcient anisotropic advection-diffusion equation.


This code was used to get some numerical results presented in a [proceeding](https://hal.science/hal-04036599) as well as an [article](https://hal.science/hal-04250412) about structure-preservation with high-order scheme. 

#### Installation
 
The following softwares/libraries are required for compilation:

* C++
* CMake
* Eigen
* Boost 

To compile the code, you should create a build directory.


mkdir build                 <br>
cd build                    <br>
cmake ..                    <br>
make                        

The compilation will create a bin directory, in which you can find and execute "hho_expfitt_evol".
In order to get some data (visualisation files), you should create an "output" directory in the build directory.

mkdir output                <br>
./bin/hho_expfitt_evol      <br>


### Getting started


When running the code, you will be prompted to select a mesh, a final time and a time step.

The data of the problem (initial condition, source term, boundary values, physical data) can be specified in the ExactSolution constructor (file Data.h).
Some specific test case data are commented in this file.<br>

The visualisation files (.vtu format) can be read directcly using Paraview. 
The other generated files (time_hho_k and temps_long_hhok) are data files about the long-time behaviour.


### Authors 

Author: Julien Moatti (julien.moatti@tuwien.ac.at)

### License

GNU General Public License v3.0

### Project status

This has been created for personnal use.
There might or might not be updates in the future.




