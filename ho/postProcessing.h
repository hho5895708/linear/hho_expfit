// -*- C++ -*-
#ifndef POSTPROCESSING_H
#define POSTPROCESSING_H

#include "Mesh/Mesh.h"
#include "VectorBasis.h"

#include <Eigen/Dense>

namespace ho {

  template<std::size_t N>
  struct PlotNodes
  {
  };

  template<>
  struct PlotNodes<1>
  {
    static const std::size_t nb_nodes = 3;
    static const std::size_t nb_subel = 1;

    std::array<Eigen::Matrix<double, 2, 1>, nb_nodes> nodes;
    std::array<Eigen::Matrix<std::size_t, 3, 1>, nb_subel> connectivity;

    PlotNodes() {
      nodes[0] << 0., 0.;
      nodes[1] << 1., 0.;
      nodes[2] << 0., 1.;

      connectivity[0] << 0, 1, 2;
    }
  };

  template<>
  struct PlotNodes<2>
  {
    static const std::size_t nb_nodes = 6;
    static const std::size_t nb_subel = 4;

    std::array<Eigen::Matrix<double, 2, 1>, nb_nodes> nodes;
    std::array<Eigen::Matrix<std::size_t, 3, 1>, nb_subel> connectivity;

    PlotNodes() {
      nodes[0] << 0., 0.;
      nodes[1] << 1., 0.;
      nodes[2] << 0., 1.;
      nodes[3] << 0.5, 0.;
      nodes[4] << 0.5, 0.5;
      nodes[5] << 0, 0.5;

      connectivity[0] << 0, 3, 5;
      connectivity[1] << 3, 1, 4;
      connectivity[2] << 3, 4, 5;
      connectivity[3] << 5, 4, 2;
    }
  };

  template<>
  struct PlotNodes<3>
  {
    static const std::size_t nb_nodes = 10;
    static const std::size_t nb_subel = 9;

    std::array<Eigen::Matrix<double, 2, 1>, nb_nodes> nodes;
    std::array<Eigen::Matrix<std::size_t, 3, 1>, nb_subel> connectivity;

    PlotNodes() {
      nodes[0] << 0., 0.;
      nodes[1] << 1., 0.;
      nodes[2] << 0., 1.;
      nodes[3] << 1./3., 0.;
      nodes[4] << 2./3., 0.;
      nodes[5] << 2./3., 1./3.;
      nodes[6] << 1./3., 2./3.;
      nodes[7] << 0., 2./3.;
      nodes[8] << 0., 1./3.;
      nodes[9] << 1./3., 1./3.;

      connectivity[0] << 0, 3, 8;
      connectivity[1] << 3, 4, 9;
      connectivity[2] << 3, 9, 8;
      connectivity[3] << 8, 9, 7;
      connectivity[4] << 4, 1, 5;
      connectivity[5] << 4, 5, 9;
      connectivity[6] << 9, 5, 6;
      connectivity[7] << 9, 6, 7;
      connectivity[8] << 7, 6, 2;
    }
  };

  //------------------------------------------------------------------------------

  template<typename BasisType, std::size_t N>
  void postProcessing_poly(const char * file_name,
                      const Mesh * Th,
                      const std::vector<Eigen::VectorXd> & coeffs)
  {
    ho::PlotNodes<N> plot_nodes;

    std::array<std::stringstream, BasisType::dim> u;
    for(std::size_t i = 0; i < BasisType::dim; i++) {
      u[i] << "<DataArray type=\"Float32\" Name=\"U" << i << "\" format=\"ascii\">" << std::endl;
    } // for i

    std::stringstream points;
    points << "<Points>" << std::endl;
    points << "<DataArray type=\"Float32\" NumberOfComponents=\"3\" format=\"ascii\">" << std::endl;

    std::stringstream connectivity;
    connectivity << "<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">" << std::endl;

    std::size_t nb_points = 0, nb_cells = 0;

    for(int iT = 0; iT < Th->numberOfCells(); iT++) {
      const Cell & T = Th->cell(iT);
      const Point & xT = T.center();

      // Estimate hT
      Real hT = cell_diameter(Th, iT);

      // Construct local basis
      BasisType basisT(T.center(), hT);

      // Retrieve coefficients
      const Eigen::VectorXd & coeffs_T = coeffs[iT];
      assert(BasisType::size == coeffs_T.size());

      for(int iF_loc = 0; iF_loc < T.numberOfFaces(); iF_loc++) {
        const Face & F = Th->face(T.faceId(iF_loc));
        const auto & points_F = F.points();

        auto geomap = [xT, points_F](const Eigen::Matrix<double, 2, 1> & x) -> Point {
          return xT + (points_F[0] - xT) * x(0) + (points_F[1] - xT) * x(1);
        };

        for(std::size_t iC = 0; iC < ho::PlotNodes<N>::nb_subel; iC++) {
          for(std::size_t iP = 0; iP < (std::size_t)plot_nodes.connectivity[iC].size(); iP++) {
            connectivity << nb_points + plot_nodes.connectivity[iC](iP) << " " << std::flush;
          } // for iP
          connectivity << std::endl;
          nb_cells++;
        } // for iC

        for(std::size_t iP = 0; iP < ho::PlotNodes<N>::nb_nodes; iP++) {
          Point xP = geomap(plot_nodes.nodes[iP]);

          points << xP(0) << " " << xP(1) << " " << 0. << std::endl;

          Eigen::Matrix<Real, BasisType::dim, 1> u_P = Eigen::Matrix<Real, BasisType::dim, 1>::Zero();
          for(int i = 0; i < BasisType::size; i++) {
            Eigen::Matrix<Real, BasisType::dim, 1> u_P_i; u_P_i << coeffs_T(i) * basisT.phi(i).phi(xP);
            u_P += u_P_i;
          } // for i

          for(std::size_t i = 0; i < BasisType::dim; i++) {
            u[i] << u_P(i) << " " << std::flush;
          } // for i

          nb_points++;
        } // for iP
        // points << std::endl;
      } // for iF_loc
    } // for iT

    for(std::size_t i = 0; i < BasisType::dim; i++) {
      u[i] << "</DataArray>" << std::endl;
    } // for i

    points << "</DataArray>" << std::endl;
    points << "</Points>" << std::endl;

    connectivity << "</DataArray>" << std::endl;

    // Write output file
    std::ofstream file(file_name, std::ios::out);

    file.flags(std::ios_base::scientific);

    file << "<?xml version=\"1.0\"?>" << std::endl;
    file << "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\" compressor=\"vtkZLibDataCompressor\">" << std::endl;
    file << "<UnstructuredGrid>" << std::endl;

    file << "<Piece NumberOfPoints=\"" << nb_points << "\" NumberOfCells=\"" << nb_cells << "\">" << std::endl;

    file << "<PointData Scalars=\"" << std::flush;
    for(std::size_t i = 0; i < BasisType::dim; i++) {
      file << "U" << i << " " << std::flush;
    } // for i
    file << "\">" << std::endl;

    for(std::size_t i = 0; i < BasisType::dim; i++) {
      file << u[i].str() << std::endl;
    } // for i
    file << "</PointData>" << std::endl;

    file << points.str() << std::endl;

    file << "<Cells>" << std::endl;
    file << connectivity.str() << std::endl;

    file << "<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">" << std::endl;
    for(std::size_t iC = 1; iC <= nb_cells; iC++) { file << iC * 3 << " " << std::flush; }
    file << std::endl;
    file << "</DataArray>" << std::endl;

    file << "<DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\">" << std::endl;
    for(std::size_t iC = 0; iC < nb_cells; iC++) { file << 5 << " " << std::flush; }
    file << std::endl;
    file << "</DataArray>" << std::endl;
    file << "</Cells>" << std::endl;

    file << "</Piece>" << std::endl;
    file << "</UnstructuredGrid>" << std::endl;
    file << "</VTKFile>" << std::endl;
  }
 
  template<typename BasisType, std::size_t N>
  void postProcessing_expfitt(const char * file_name,
                      const Mesh * Th,
                      const FctContType & omega,
                      const std::vector<Eigen::VectorXd> & coeffs_rho)
  {
    ho::PlotNodes<N> plot_nodes;

    std::array<std::stringstream, BasisType::dim> u;
    for(std::size_t i = 0; i < BasisType::dim; i++) {
      u[i] << "<DataArray type=\"Float32\" Name=\"U" << i << "\" format=\"ascii\">" << std::endl;
    } // for i

    std::stringstream points;
    points << "<Points>" << std::endl;
    points << "<DataArray type=\"Float32\" NumberOfComponents=\"3\" format=\"ascii\">" << std::endl;

    std::stringstream connectivity;
    connectivity << "<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">" << std::endl;

    std::size_t nb_points = 0, nb_cells = 0;

    for(int iT = 0; iT < Th->numberOfCells(); iT++) {
      const Cell & T = Th->cell(iT);
      const Point & xT = T.center();

      // Estimate hT
      Real hT = cell_diameter(Th, iT);

      // Construct local basis
      BasisType basisT(T.center(), hT);

      // Retrieve coefficients
      const Eigen::VectorXd & coeffs_T = coeffs_rho[iT];
      assert(BasisType::size == coeffs_T.size());

      for(int iF_loc = 0; iF_loc < T.numberOfFaces(); iF_loc++) {
        const Face & F = Th->face(T.faceId(iF_loc));
        const auto & points_F = F.points();

        auto geomap = [xT, points_F](const Eigen::Matrix<double, 2, 1> & x) -> Point {
          return xT + (points_F[0] - xT) * x(0) + (points_F[1] - xT) * x(1);
        };

        for(std::size_t iC = 0; iC < ho::PlotNodes<N>::nb_subel; iC++) {
          for(std::size_t iP = 0; iP < (std::size_t)plot_nodes.connectivity[iC].size(); iP++) {
            connectivity << nb_points + plot_nodes.connectivity[iC](iP) << " " << std::flush;
          } // for iP
          connectivity << std::endl;
          nb_cells++;
        } // for iC

        for(std::size_t iP = 0; iP < ho::PlotNodes<N>::nb_nodes; iP++) {
          Point xP = geomap(plot_nodes.nodes[iP]);

          points << xP(0) << " " << xP(1) << " " << 0. << std::endl;

          Eigen::Matrix<Real, BasisType::dim, 1> u_P = Eigen::Matrix<Real, BasisType::dim, 1>::Zero();
          for(int i = 0; i < BasisType::size; i++) {
            Eigen::Matrix<Real, BasisType::dim, 1> u_P_i; u_P_i <<  coeffs_T(i) * omega(xP) * basisT.phi(i).phi(xP);
            u_P += u_P_i;
          } // for i

          for(std::size_t i = 0; i < BasisType::dim; i++) {
            u[i] << u_P(i) << " " << std::flush;
          } // for i

          nb_points++;
        } // for iP
        // points << std::endl;
      } // for iF_loc
    } // for iT

    for(std::size_t i = 0; i < BasisType::dim; i++) {
      u[i] << "</DataArray>" << std::endl;
    } // for i

    points << "</DataArray>" << std::endl;
    points << "</Points>" << std::endl;

    connectivity << "</DataArray>" << std::endl;

    // Write output file
    std::ofstream file(file_name, std::ios::out);

    file.flags(std::ios_base::scientific);

    file << "<?xml version=\"1.0\"?>" << std::endl;
    file << "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\" compressor=\"vtkZLibDataCompressor\">" << std::endl;
    file << "<UnstructuredGrid>" << std::endl;

    file << "<Piece NumberOfPoints=\"" << nb_points << "\" NumberOfCells=\"" << nb_cells << "\">" << std::endl;

    file << "<PointData Scalars=\"" << std::flush;
    for(std::size_t i = 0; i < BasisType::dim; i++) {
      file << "U" << i << " " << std::flush;
    } // for i
    file << "\">" << std::endl;

    for(std::size_t i = 0; i < BasisType::dim; i++) {
      file << u[i].str() << std::endl;
    } // for i
    file << "</PointData>" << std::endl;

    file << points.str() << std::endl;

    file << "<Cells>" << std::endl;
    file << connectivity.str() << std::endl;

    file << "<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">" << std::endl;
    for(std::size_t iC = 1; iC <= nb_cells; iC++) { file << iC * 3 << " " << std::flush; }
    file << std::endl;
    file << "</DataArray>" << std::endl;

    file << "<DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\">" << std::endl;
    for(std::size_t iC = 0; iC < nb_cells; iC++) { file << 5 << " " << std::flush; }
    file << std::endl;
    file << "</DataArray>" << std::endl;
    file << "</Cells>" << std::endl;

    file << "</Piece>" << std::endl;
    file << "</UnstructuredGrid>" << std::endl;
    file << "</VTKFile>" << std::endl;
  }

} // namespace ho
#endif
