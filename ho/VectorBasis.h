// -*- C++ -*-
#ifndef VECTORBASIS_H
#define VECTORBASIS_H

#include <array>
#include <functional>

#include <boost/math/special_functions/binomial.hpp>

#include "Common/defs.h"

#include "Basis.h"

#include <Eigen/Dense>

namespace ho
{
  template<int K>
  class HierarchicalScalarBasis2d
  {
  public:
    typedef Real ValueType;
    typedef Eigen::Matrix<Real, 2, 1> GradientType;

    typedef  std::function<ValueType(const Point &)> BasisFunctionType;
    typedef  std::function<GradientType(const Point &)> BasisGradientType;

    struct BasisFunction 
    {
      BasisFunctionType phi;
      BasisGradientType dphi;
    };

    enum {
      dim    = 1,
      degree = K,
      size   = detail::Binomial<K+2,2>::value
    };

    typedef std::array<BasisFunction, HierarchicalScalarBasis2d<K>::size> BasisFunctionArray;

    HierarchicalScalarBasis2d(const Point & xT, const Real & hT);

    inline const BasisFunctionArray & Phi() const { return m_Phi; }
    inline const BasisFunction & phi(int i) const { return m_Phi[i]; }
    std::size_t degreeIndex(const std::size_t & k) const { return m_degree_index(k); }
  
  private:
    Point m_xT;
    Real m_hT;
    BasisFunctionArray m_Phi;
    Eigen::Array<std::size_t, HierarchicalScalarBasis2d<K>::size+1, 1> m_degree_index;
  };


  //------------------------------------------------------------------------------

  template<int K>
  class HierarchicalVectorBasis2d
  {
  public:
    typedef Eigen::Matrix<Real, 2, 1> ValueType;
    typedef Eigen::Matrix<Real, 2, 2> GradientType;
    
    typedef std::function<ValueType(const Point &)> BasisFunctionType;
    typedef std::function<GradientType(const Point &)> BasisGradientType;

    struct BasisFunction
    {
      BasisFunctionType phi;
      BasisGradientType dphi;
    };

    enum { 
      dim    = 2,
      degree = K,
      size   = 2 * detail::Binomial<K+2,2>::value
    };

    typedef std::array<BasisFunction, HierarchicalVectorBasis2d<K>::size> BasisFunctionArray;

    HierarchicalVectorBasis2d(const Point & xT, const Real & hT);

    inline const BasisFunctionArray & Phi() const { return m_Phi; }
    inline const BasisFunction & phi(int i) const { return m_Phi[i]; }
    std::size_t degreeIndex(const std::size_t & k) const { return m_degree_index(k); }

  private:
    Point m_xT;
    Real m_hT;
    BasisFunctionArray m_Phi;
    Eigen::Array<std::size_t, HierarchicalVectorBasis2d<K>::size+1, 1> m_degree_index;
  };

  //------------------------------------------------------------------------------

  template<int K>
  class HierarchicalScalarBasis1d
  {
  public:
    typedef Real ValueType;
    typedef std::function<ValueType(const Point &)> BasisFunctionType;

    struct BasisFunction
    {
      BasisFunctionType phi;
    };

    enum { 
      degree = K,
      size   = K+1
    };

    typedef std::array<BasisFunction, HierarchicalScalarBasis1d<K>::size> BasisFunctionArray;

    HierarchicalScalarBasis1d(const Point & x0, const Point & xF, const Real & hF);

    inline const BasisFunctionArray & Phi() const { return m_Phi; }
    inline const BasisFunction & phi(std::size_t i) const { return m_Phi[i]; };
    std::size_t degreeIndex(const std::size_t & k) const { return k; }

  private:
    Point m_x0;
    Point m_xF;
    Real m_hF;
    BasisFunctionArray m_Phi;
  };

  //------------------------------------------------------------------------------

  template<int K>
  class HierarchicalVectorBasis1d
  {
  public:
    typedef Eigen::Matrix<Real, 2, 1> ValueType;
    typedef std::function<ValueType(const Point &)> BasisFunctionType;

    struct BasisFunction
    {
      BasisFunctionType phi;
    };

    enum { 
      degree = K,
      size   = 2 * (K+1)
    };

    typedef std::array<BasisFunction, HierarchicalVectorBasis1d<K>::size> BasisFunctionArray;

    HierarchicalVectorBasis1d(const Point & x0, const Point & xF, const Real & hF);

    inline const BasisFunctionArray & Phi() const { return m_Phi; }
    inline const BasisFunction & phi(std::size_t i) const { return m_Phi[i]; };
    std::size_t degreeIndex(const std::size_t & k) const { return 2 * k; }

  private:
    Point m_x0;
    Point m_xF;
    Real m_hF;
    BasisFunctionArray m_Phi;
  };

  //------------------------------------------------------------------------------

  // Warning: first and last here refer to the polynomial degree instead of the
  // index of the basis function

  template<typename BasisType>
  class BasisFunctionEvaluation {
  public:
    BasisFunctionEvaluation(const BasisType * B,
                            const std::vector<Point> & points,
                            std::size_t first = 0,
                            std::size_t last = BasisType::degree);

    inline const typename BasisType::ValueType & operator()(std::size_t i, std::size_t j) const 
    { 
      return m_phi(i, j); 
    }

  private:
    Eigen::Array<typename BasisType::ValueType, Eigen::Dynamic, Eigen::Dynamic> m_phi;
  };

  //------------------------------------------------------------------------------
  
  template<typename BasisType>
  class BasisGradientEvaluation {
  public:
    BasisGradientEvaluation(const BasisType * B,
                            const std::vector<Point> & points,
                            std::size_t first = 0,
                            std::size_t last = BasisType::degree);

    inline const typename BasisType::GradientType & operator()(std::size_t i, std::size_t j) const 
    { 
      return m_dphi(i,j); 
    }

  private:
    Eigen::Array<typename BasisType::GradientType, Eigen::Dynamic, Eigen::Dynamic> m_dphi;
  };

  //------------------------------------------------------------------------------
  // Implementation

  //------------------------------------------------------------------------------
  // HierarchicalScalarBasis2d

  template<int K>
  HierarchicalScalarBasis2d<K>::HierarchicalScalarBasis2d(const Point & xT, const Real & hT)
    : m_xT(xT)
    , m_hT(hT)
  {
    int i_phi = 0;
    for(int k = 0; k <= K; k++) {
      m_degree_index(k) = i_phi;
      for(int i = 0; i <= k; i++, i_phi++) {
        BasisFunctionType phi = [i,k,this](const Point & x) -> Real { 
          Real _x = (x(0)-this->m_xT(0))/this->m_hT;
          Real _y = (x(1)-this->m_xT(1))/this->m_hT;
          return std::pow(_x,i) * std::pow(_y,k-i);
        };
        BasisGradientType dphi = [i,k,this](const Point & x) -> GradientType {
          GradientType g;
          Real _x = (x(0)-this->m_xT(0))/this->m_hT;
          Real _y = (x(1)-this->m_xT(1))/this->m_hT;
          g(0) = (i == 0) ? 0. : i*std::pow(_x,i-1)/m_hT * std::pow(_y,k-i);
          g(1) = (i == k) ? 0. : std::pow(_x,i) * (k-i) * std::pow(_y,k-i-1)/m_hT;
          return g;
        };
        m_Phi[i_phi].phi = phi;
        m_Phi[i_phi].dphi = dphi;
      } // for i
    } // for k
    m_degree_index(K+1) = i_phi;
  }

  //------------------------------------------------------------------------------
  // HierarchicalVectorBasis2d

  template<int K>
  HierarchicalVectorBasis2d<K>::HierarchicalVectorBasis2d(const Point & xT, const Real & hT)
    : m_xT(xT)
    , m_hT(hT)
  {
    int i_phi = 0;
    // Pascal triangle's row
    for(std::size_t k = 0; k <= K; k++) {
      m_degree_index(k) = i_phi;
      // Component
      for(std::size_t ic = 0; ic < 2; ic++) {
        // Exponent for x(0)
        for(std::size_t i = 0; i <= k; i++, i_phi++) {
          BasisFunctionType phi = [k,ic,i,this](const Point & x) -> ValueType {
            Real _x = (x(0)-this->m_xT(0))/this->m_hT;
            Real _y = (x(1)-this->m_xT(1))/this->m_hT;
            ValueType res = ValueType::Zero();
            res(ic) = std::pow(_x,i) * std::pow(_y,k-i);
            return res;
          };
          BasisGradientType dphi = [k,ic,i,this](const Point & x) -> GradientType {
            Real _x = (x(0)-this->m_xT(0))/this->m_hT;
            Real _y = (x(1)-this->m_xT(1))/this->m_hT;
            GradientType res = GradientType::Zero();
            res(ic,0) = (i == 0) ? 0. : i*std::pow(_x,i-1)/m_hT * std::pow(_y,k-i);
            res(ic,1) = (i == k) ? 0. : std::pow(_x,i) * (k-i) * std::pow(_y,k-i-1)/m_hT;
            return res;
          };
          assert(i_phi < m_Phi.size());
          m_Phi[i_phi].phi = phi;
          m_Phi[i_phi].dphi = dphi;
        } // for i
      } // for ic
    } // for k
    m_degree_index(K+1) = i_phi;
  }

  //------------------------------------------------------------------------------
  // HierarchicalScalarBasis1d

  template<int K>
  HierarchicalScalarBasis1d<K>::HierarchicalScalarBasis1d(const Point & x0, const Point & xF, const Real & hF)
    : m_x0(x0)
    , m_xF(xF)
    , m_hF(hF)
  {
    // Degree
    for(std::size_t k = 0; k <= K; k++) {
      BasisFunctionType phi = [k,this](const Point & x) -> ValueType {
        Real d = inner_prod(x-this->m_xF, this->m_x0-this->m_xF)/(std::pow(this->m_hF, 2));
        return std::pow(d, k);
      };
      m_Phi[k].phi = phi;
    } // for k
  }
  
  //------------------------------------------------------------------------------
  // HierarchicalVectorBasis1d

  template<int K>
  HierarchicalVectorBasis1d<K>::HierarchicalVectorBasis1d(const Point & x0, const Point & xF, const Real & hF)
    : m_x0(x0)
    , m_xF(xF)
    , m_hF(hF)
  {
    // Degree
    std::size_t i_phi = 0;
    for(std::size_t k = 0; k <= K; k++) {
      // Component
      for(std::size_t ic = 0; ic < 2; ic++, i_phi++) {
        BasisFunctionType phi = [k,ic,this](const Point & x) -> ValueType {
          Real d = inner_prod(x-this->m_xF, this->m_x0-this->m_xF)/(std::pow(this->m_hF, 2));
          ValueType res = ValueType::Zero();
          res(ic) = std::pow(d, k);
          return res;
        };
        m_Phi[i_phi].phi = phi;
      } // for k
    } // for ic
  }

  //------------------------------------------------------------------------------
  // BasisFunctionEvaluation

  template<typename BasisType>
  BasisFunctionEvaluation<BasisType>::BasisFunctionEvaluation(const BasisType * B,
                                                              const std::vector<Point> & points,
                                                              std::size_t first,
                                                              std::size_t last)
  {
    assert(last >= first && first >= 0 && last <= BasisType::degree);

    int offset = B->degreeIndex(last+1) - B->degreeIndex(first);
    int nb_points = points.size();
    
    m_phi.resize(offset, nb_points);
    
    for(int l = 0; l < nb_points; l++) {
      const Point & xl = points[l];

      std::size_t i_phi = 0;
      for(std::size_t i = B->degreeIndex(first); i < B->degreeIndex(last+1); i++, i_phi++) {
        m_phi(i_phi, l) = B->phi(i).phi(xl);
      } // for i
    } // for l
  }

  //------------------------------------------------------------------------------
  // BasisFunctionEvaluation

  template<typename BasisType>
  BasisGradientEvaluation<BasisType>::BasisGradientEvaluation(const BasisType * B,
                                                              const std::vector<Point> & points,
                                                              std::size_t first,
                                                              std::size_t last)
  {
    assert(last >= first && first >= 0 && last <= BasisType::degree);

    int offset = B->degreeIndex(last+1) - B->degreeIndex(first);
    int nb_points = points.size();

    m_dphi.resize(offset, nb_points);
    
    for(int l = 0; l < nb_points; l++) {
      const Point & xl = points[l];

      std::size_t i_phi = 0;
      for(std::size_t i = B->degreeIndex(first); i < B->degreeIndex(last+1); i++, i_phi++) {
        m_dphi(i_phi, l) = B->phi(i).dphi(xl);
      } // for i
    } // for l
  }

} // namespace ho

#endif
