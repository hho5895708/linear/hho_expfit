// -*- C++ -*-
#ifndef PHO_H
#define PHO_H

#ifndef add_integrator
#define add_integrator 3
#endif

#include "VectorBasis.h"
#include "QuadratureRules/FaceIntegrator.h"
#include "QuadratureRules/PyramidIntegrator.h"

#include <chrono>

typedef Eigen::Matrix<std::size_t, Eigen::Dynamic, 1> IndexVectorType;

typedef Eigen::Matrix<Real, 2, 2 > TensorType;
typedef std::function<TensorType(const Point &)> DiffusivityType;
typedef std::function<Real(const Point &)> PotentialType;
typedef std::function<Eigen::Matrix<Real, 2, 1>(const Point &)> FieldType;
typedef std::function<Real(const Point &)> LoadType;
typedef std::function<Real(const Point &)> FctContType;
typedef std::function<Real(const Real &)> RealFctType;
typedef std::function<Real(const Point &)> PotentialType;
typedef std::function<Real(const Point &)> InitialDataType;


typedef std::function<Real(const Point &)> BoundaryDirichletType;
typedef std::function<Real(const Point &)> BoundaryNeumannType;

typedef std::function<bool(const Face &)> DefBoundaryConditionType;    

typedef std::function<Real(const Point &)> ExactSolutionType;
typedef std::function<Eigen::Matrix<Real, DIM, 1>(const Point &)> ExactGradientType;


namespace ho
{

  namespace pho
  {
    template<std::size_t K>
    struct LocalContributions_ExpFitt_time
    {
      typedef ho::HierarchicalScalarBasis2d<K+1> CellBasis; 
      typedef ho::HierarchicalScalarBasis1d<K> FaceBasis;
      typedef ho::HierarchicalVectorBasis2d<K> CellVecBasis; 

      typedef std::function<typename CellBasis::ValueType(const Point &)> ExactSolutionType;
      typedef std::function<typename CellBasis::ValueType(const Point &)> LoadType;
      typedef std::function<TensorType(const Point &)> DiffusivityType;

      // Initialize and compute local operators
      LocalContributions_ExpFitt_time(const Mesh * Th,
                         const Integer & iT,
                         const FctContType & omega, 
                         const Real & dt, 
                         const LoadType & load,
                         const BoundaryNeumannType & g_n,
                         const DiffusivityType & Lambda, 
                         const DefBoundaryConditionType & isNeu,
                         const Real & eta = 1.);
      // Interpolate exact solution
      Eigen::VectorXd interpolate(const Mesh * Th, const Integer & iT, const ExactSolutionType & u) const;
      // Reconstruct local solution
      template<class SolutionVectorType, class IndexVectorType>
      Eigen::VectorXd reconstruct_time( const Mesh * Th,
                                        const Integer & iT,  
                                        const Real & dt,
                                        const Eigen::VectorXd & U_n,
                                        const SolutionVectorType & X,
                                        const IndexVectorType & idx_T) const;       
      template<class SolutionVectorType, class IndexVectorType>
      Eigen::VectorXd reconstruct_time_first_step( const Mesh * Th,
                                        const Integer & iT,  
                                        const Real & dt,
                                        const ExactSolutionType & u_0,
                                        const SolutionVectorType & X,
                                        const IndexVectorType & idx_T) const;      
      // Compute the term from the previous time step (in the Euler scheme)
      //template<class SolutionVectorType, class IndexVectorType>
      Eigen::VectorXd compute_previous_time_contrib(const Mesh * Th, 
                                                    const Real & dt, 
                                                    const Integer & iT,
                                                    const Eigen::VectorXd & rho_T) const;    
      Eigen::VectorXd compute_previous_time_contrib_first_step(const Mesh * Th, 
                                                  const Real & dt, 
                                                  const Integer & iT,
                                                  const ExactSolutionType & u_0) const;
      template<class SolutionVectorType, class IndexVectorType>
      void Compute_diff_L2_H1(const Mesh * Th,
                                  const Integer & iT,
                                  const IndexVectorType & idx_T,
                                  const FctContType & omega,
                                  const PotentialType & Phi,
                                  const SolutionVectorType & X_rho,
                                  const ExactSolutionType & u_ex,
                                  const ExactGradientType & Grad_ex,
                                  Real & L2_square,
                                  Real & H1_square,
                                  Real & min_cell,
                                  Real & min_faces ) const;  
          
      void Compute_long_time( const Mesh * Th,
                              const Integer & iT,
                              const FctContType & omega,
                              const Eigen::VectorXd & rho_T,
                              const Real & rho_eq,
                              Real & L2_square,
                              Real & L1_square,
                              Real & min_cell,
                              int & nb_neg_p ,
                              int & nb_neg_c ) const;       
      template<class SolutionVectorType, class IndexVectorType>
      void Compute_mins(  const Mesh * Th,
                          const Integer & iT,
                          const FctContType & omega,
                          const Eigen::VectorXd & rho_T,
                          const SolutionVectorType & X,
                          const IndexVectorType & idx_T,
                          Real & min_cell_mean,
                          Real & min_faces_mean,
                          Real & min_QN_cells,
                          Real & min_QN_faces,                    
                          int & nb_neg_cells_mean) const; 
     
      // // High-order potential reconstruction
      // Eigen::Matrix<Real, ho::HierarchicalScalarBasis2d<K+1>::size, 1>
      // potentialReconstruction(const Mesh * Th,
      //                         const Integer & iT,
      //                         const Eigen::VectorXd & uh_TF);

     
      // fullDiscreteGradient(const Mesh * Th,
      //                         const Integer & iT,
      //                         const Eigen::VectorXd & uh_TF);
      

      enum {
        nb_cell_dofs       = ho::HierarchicalScalarBasis2d<K+1>::size,
        nb_local_cell_dofs = ho::HierarchicalScalarBasis2d<K+1>::size,
        nb_local_face_dofs = ho::HierarchicalScalarBasis1d<K>::size,
        nb_vec_cell_dofs   = ho::HierarchicalVectorBasis2d<K>::size, 
        nb_vec_face_dofs   = ho::HierarchicalVectorBasis1d<K>::size, 
        NG_full            = ho::HierarchicalVectorBasis2d<K>::size
      };

      std::shared_ptr<CellBasis> basisT; 			
      std::shared_ptr<CellVecBasis> basis_vec_T; 					
      std::vector<std::shared_ptr<FaceBasis> > basisF;
      	
      Eigen::MatrixXd G_full_T; // discrete full gradient 
      Eigen::MatrixXd STF; // Stabilization

      Eigen::MatrixXd ATF; // matrix of the local bilinear form 
      Eigen::VectorXd bTF; // vector of local rhs (contributions from int_T f and Neumann int_F g^N) 

      // RowMajor format is needed for compatibility with PETSc
      Eigen::LDLT<Eigen::MatrixXd> LU_ATT_omega; // LU decomposition of the top left block of ATF ( ~ cells) -> LU_ATT.solve(rhs)
      Eigen::Matrix<Real, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> AF; // Schur complement of ATF ("condensed version")
      Eigen::VectorXd bF; // vector of condensed BFT (-> contribution fom the loading term +  Neumann BC )

      Eigen::Matrix<Real, CellBasis::size, CellBasis::size> MTT; // local mass matrix of the cell 
      Eigen::LDLT<Eigen::MatrixXd> LU_MTT; //LU decomposition of MTT -> LU_MTT.solve(rhs)      
      Eigen::Matrix<Real, CellBasis::size, CellBasis::size> MTT_omega; // local mass matrix of the cell for the omega-weighted scalar

      std::vector<Eigen::Matrix<Real, FaceBasis::size, FaceBasis::size> > MFF; //vector of local "mass" matrices of the faces 
      std::vector<Eigen::LLT<Eigen::MatrixXd> > LU_MFF; //vector of the decomposition (Cholesky in fact)
      // of the local "mass" matrices on the faces  -> LU_MFF[iF_loc].solve(rhs) 
      
      std::vector<Eigen::Matrix<Real, FaceBasis::size, CellBasis::size> > MFT;	
            
      
      // Eigen::MatrixXd GT;
     

      std::size_t nb_tot_faces_dofs;
      std::size_t nb_tot_dofs;

      // Basis evaluation at
      std::vector<std::shared_ptr<BasisFunctionEvaluation<CellBasis> > > feval_basisT_PTF; 
      std::vector<std::shared_ptr<BasisGradientEvaluation<CellBasis> > > deval_basisT_PTF;
      std::vector<std::shared_ptr<BasisFunctionEvaluation<CellBasis> > > feval_basisT_F;
      std::vector<std::shared_ptr<BasisGradientEvaluation<CellBasis> > > deval_basisT_F;
      std::vector<std::shared_ptr<BasisFunctionEvaluation<FaceBasis> > > feval_basisF;
      std::vector<std::shared_ptr<BasisFunctionEvaluation<CellVecBasis> > > feval_vec_basisT_PTF;
      std::vector<std::shared_ptr<BasisFunctionEvaluation<CellVecBasis> > > feval_vec_basisT_F;

      std::vector<std::shared_ptr<PyramidIntegrator> > pims;		
      std::vector<std::shared_ptr<FaceIntegrator> > fims;
    };

    //------------------------------------------------------------------------------
    // Implementation

    template<std::size_t K>
    LocalContributions_ExpFitt_time<K>::LocalContributions_ExpFitt_time(
                         const Mesh * Th,
                         const Integer & iT,
                         const FctContType & omega, 
                         const Real & dt, 
                         const LoadType & load,
                         const BoundaryNeumannType & g_n,
                         const DiffusivityType & Lambda, 
                         const DefBoundaryConditionType & isNeu,
                         const Real & eta )
    {
      const Cell & T = Th->cell(iT);
      const Point & xT = T.center();

      // Estimate hT
      Real hT = cell_diameter(Th, iT);

      std::vector<Real> Lambda_intensity; 

      //------------------------------------------------------------------------------
      // Precompute bases

      basisT.reset(new CellBasis(T.center(), hT));		
      basis_vec_T.reset(new CellVecBasis(T.center(), hT));		
      basisF.resize(T.numberOfFaces());
      for(int iF_loc = 0; iF_loc < T.numberOfFaces(); iF_loc++) {
        const Face & F = Th->face(T.faceId(iF_loc));	
        const Point & xF = F.barycenter();		
        const Real & hF = F.measure();	
        basisF[iF_loc].reset(new FaceBasis(F.point(0).first, xF, hF));
      }

      // Evaluate bases at quadrature nodes
      feval_basisT_PTF.resize(T.numberOfFaces());
      deval_basisT_PTF.resize(T.numberOfFaces());
      feval_basisT_F.resize(T.numberOfFaces());
      deval_basisT_F.resize(T.numberOfFaces());
      feval_basisF.resize(T.numberOfFaces());     
      feval_vec_basisT_PTF.resize(T.numberOfFaces());
      feval_vec_basisT_F.resize(T.numberOfFaces());

      pims.resize(T.numberOfFaces());
      fims.resize(T.numberOfFaces());

      for(int iF_loc = 0; iF_loc < T.numberOfFaces(); iF_loc++) {
        Integer iF = T.faceId(iF_loc);			

        pims[iF_loc].reset(new PyramidIntegrator(Th, iT, iF_loc, 2*(K+1) + add_integrator));
        const PyramidIntegrator & pim = *pims[iF_loc];

        feval_basisT_PTF[iF_loc].reset(new BasisFunctionEvaluation<CellBasis>(basisT.get(), pim.points()));
        deval_basisT_PTF[iF_loc].reset(new BasisGradientEvaluation<CellBasis>(basisT.get(), pim.points()));
        feval_vec_basisT_PTF[iF_loc].reset(new BasisFunctionEvaluation<CellVecBasis>(basis_vec_T.get(), pim.points()));

        fims[iF_loc].reset(new FaceIntegrator(Th, iF, 2*(K+1) + add_integrator  ));	
        const FaceIntegrator & fim = *fims[iF_loc];
        feval_basisT_F[iF_loc].reset(new BasisFunctionEvaluation<CellBasis>(basisT.get(), fim.points()));
        deval_basisT_F[iF_loc].reset(new BasisGradientEvaluation<CellBasis>(basisT.get(), fim.points(), 1));
        feval_basisF[iF_loc].reset(new BasisFunctionEvaluation<FaceBasis>(basisF[iF_loc].get(), fim.points()));      
        feval_vec_basisT_F[iF_loc].reset(new BasisFunctionEvaluation<CellVecBasis>(basis_vec_T.get(), fim.points()));

      } // for iF_loc

      //------------------------------------------------------------------------------
      // Count local unknowns

      nb_tot_faces_dofs = T.numberOfFaces() * FaceBasis::size; 								
      nb_tot_dofs = nb_cell_dofs + nb_tot_faces_dofs;		
	
      bTF = Eigen::VectorXd::Zero(nb_tot_dofs);

      //------------------------------------------------------------------------------
      // Gradient reconstruction
      
      Eigen::MatrixXd SG = Eigen::MatrixXd::Zero(NG_full, NG_full);	
      Eigen::MatrixXd SG_Lambda = Eigen::MatrixXd::Zero(NG_full, NG_full);	
      Eigen::MatrixXd RG = Eigen::MatrixXd::Zero(NG_full, nb_tot_dofs); // matrix of the RHS of the discrete IBP formula defining the full discrete gradient 


      MTT = Eigen::Matrix<Real, CellBasis::size, CellBasis::size>::Zero(); 	
      MTT_omega = Eigen::Matrix<Real, CellBasis::size, CellBasis::size>::Zero(); 	
      MFF.resize(T.numberOfFaces());				
      LU_MFF.resize(T.numberOfFaces());

      //std::vector<Eigen::Matrix<Real, FaceBasis::size, CellBasis::size> > MFT(T.numberOfFaces());	
      MFT.resize(T.numberOfFaces());	

      for(int iF_loc = 0 ; iF_loc < T.numberOfFaces(); iF_loc++) {
        int iF = T.faceId(iF_loc);											
        const Face & F = Th->face(iF);											
        Eigen::Matrix<Real, DIM, 1> nTF;										
        { Point _nTF = F.normal(xT); nTF << _nTF(0), _nTF(1); }								
        const Real & hF = F.measure();			       
        
        Real Lambda_TF = 0.; 								

        const PyramidIntegrator & pim = *pims[iF_loc];									
        const FaceIntegrator & fim = *fims[iF_loc];

        //------------------------------------------------------------------------------
        // Initialize matrices

        MFT[iF_loc] = Eigen::Matrix<Real, FaceBasis::size, CellBasis::size>::Zero();					
        MFF[iF_loc] = Eigen::Matrix<Real, FaceBasis::size, FaceBasis::size>::Zero();		

        //------------------------------------------------------------------------------
        // Volumetric terms

        for(int iQN = 0; iQN < pim.numberOfPoints(); iQN++) {
          const Point & xQN = pim.point(iQN);										
          const Real & wQN = pim.weight(iQN);			
          const Real & omega_iqn = omega(xQN); 						          
          
          // full gradient
          for(std::size_t i = 0; i < NG_full; i++) {
            const typename CellVecBasis::ValueType & tau_i_iqn = (*feval_vec_basisT_PTF[iF_loc])(i, iQN);

            // LHS
            for(std::size_t j = 0; j< NG_full; j ++){
              const typename CellVecBasis::ValueType & tau_j_iqn = (*feval_vec_basisT_PTF[iF_loc])(j, iQN);
              SG(i,j) += wQN * tau_i_iqn.dot(tau_j_iqn);              
              const TensorType Lambda_iqn = Lambda(xQN);
              SG_Lambda(i,j) += wQN * tau_i_iqn.dot(Lambda_iqn * tau_j_iqn);
            } // for j
            

            // RHS (\GRAD vT, Tau )_{PTF}
            for(std::size_t j = 0; j < nb_cell_dofs; j++) {
              const typename CellBasis::GradientType & dphi_j_iqn = (*deval_basisT_PTF[iF_loc])(j, iQN);
              RG(i,j) += wQN * dphi_j_iqn.dot(tau_i_iqn);              
            } // for j
          } // for i
          
          
          // MTT and MTT_omega (mass matrix)
          for(std::size_t i = 0; i < CellBasis::size; i++) {
            const Real & phi_i_iqn = (*feval_basisT_PTF[iF_loc])(i, iQN);
            for(std::size_t j = 0; j < CellBasis::size; j++) {
              const Real & phi_j_iqn = (*feval_basisT_PTF[iF_loc])(j, iQN);
              MTT(i,j) += wQN * phi_i_iqn * phi_j_iqn;
              MTT_omega(i,j) += wQN * omega_iqn * phi_i_iqn * phi_j_iqn;
            } // for j
          } // for i

          // Forcing term
          for(std::size_t i = 0; i < nb_cell_dofs; i++) {
            const Real & phi_i_iqn = (*feval_basisT_PTF[iF_loc])(i, iQN);
            bTF(i) += wQN * phi_i_iqn * load(xQN);
          } // for i
        } // for iQN

        //------------------------------------------------------------------------------
        // Interface terms

        for(int iQN = 0; iQN < fim.numberOfPoints(); iQN++) {
          const Point & xQN = fim.point(iQN);
          const Real & wQN = fim.weight(iQN);

          Lambda_TF += wQN * std::pow((Lambda(xQN) * nTF).dot(nTF), 2); 

          // Offset for face unknowns
          const std::size_t offset_F = nb_cell_dofs + iF_loc * FaceBasis::size;

          // full gradient
          for(std::size_t i = 0; i < NG_full; i++) {
            const typename CellVecBasis::ValueType & tau_i_iqn = (*feval_vec_basisT_F[iF_loc])(i, iQN);
            const Real tau_i_n_iqn = tau_i_iqn.dot(nTF);

            // RHS (v_F, Tau \SCAL n_{TF})_F
            for(std::size_t j = 0; j < FaceBasis::size; j++) {
              const typename FaceBasis::ValueType & phi_j_iqn = (*feval_basisF[iF_loc])(j, iQN);
              RG(i,offset_F + j) += wQN * tau_i_n_iqn * phi_j_iqn;
            } // for j

            // RHS -(v_T, Tau \SCAL n_{TF})_F
            for(std::size_t j = 0; j < nb_cell_dofs; j++) {
              const typename CellBasis::ValueType & phi_j_iqn = (*feval_basisT_F[iF_loc])(j, iQN);
              RG(i,j) -= wQN * tau_i_n_iqn * phi_j_iqn;
            } // for j
          } // for i            
          
          // loading term associated to the Neumann BC
          if(isNeu(F)){
            for(std::size_t i = 0; i < FaceBasis::size; i++) {
              const Real & phi_i_iqn = (*feval_basisF[iF_loc])(i, iQN);
              bTF(offset_F + i)+= wQN * phi_i_iqn * g_n(xQN);  
            }//for i
          } // if F is Neumann

          // MFT (matrices "interraction between faces and cells")
          for(std::size_t i = 0; i < FaceBasis::size; i++) {
            const typename FaceBasis::ValueType & phi_i_iqn = (*feval_basisF[iF_loc])(i, iQN);
            
            for(std::size_t j = 0; j < CellBasis::size; j++) {
              const typename CellBasis::ValueType & phi_j_iqn = (*feval_basisT_F[iF_loc])(j, iQN);
              MFT[iF_loc](i,j) += wQN * phi_i_iqn * phi_j_iqn;
            } // for j
          } // for i

          // MFF ("mass" matrices associated to the faces)
          for(std::size_t i = 0; i < FaceBasis::size; i++) {
            const typename FaceBasis::ValueType & phi_i_iqn = (*feval_basisF[iF_loc])(i, iQN);
            //const auto & phi_i_iqn = (*feval_basisF[iF_loc])(i, iQN);
            for(std::size_t j = 0; j < FaceBasis::size; j++) {
              const typename FaceBasis::ValueType  & phi_j_iqn = (*feval_basisF[iF_loc])(j, iQN);
              //const auto & phi_j_iqn = (*feval_basisF[iF_loc])(j, iQN);
              MFF[iF_loc](i,j) += wQN * phi_i_iqn * phi_j_iqn;
            } // for j
          } // for i
        } // for iQN
        
        LU_MFF[iF_loc].compute(MFF[iF_loc]);
        Lambda_intensity.push_back(std::sqrt(Lambda_TF));
        
      } // for iF_loc

      //------------------------------------------------------------------------------
      // Consistent terms, gradient of the reconstruction operator

      LU_MTT.compute(MTT);
      G_full_T = SG.ldlt().solve(RG);

      /*
      const Face & F_0 = Th->face(0);	

      std::cout << "Volume cell  " << iT << " : \t"<< T.measure() << " \t Size edges :"<< F_0.measure()  << " \t Diameter :"<< hT  <<"\n" <<  std::endl;
      std::cout << "SG \n" << SG << std::endl;
      std::cout << "RG \n" << RG << std::endl;
      std::cout << "G_full \n" << G_full_T << std::endl;
      std::cout << "G \n" << GT << "\n" << std::endl;
      */
      // Add of the consistent term in the local matrix 

      //ATF = nuT * BG.transpose() * GT; 
      //ATF =  GT.transpose() * (MG_Lambda * GT) ; // with gradient of the reconstruction  
      ATF =  G_full_T.transpose() * (SG_Lambda * G_full_T) ; // full gradient 
      //------------------------------------------------------------------------------
      // Stabilization

      STF = Eigen::MatrixXd::Zero(nb_tot_dofs, nb_tot_dofs);
      
      // Matrices for extraction of the cell dofs and faces dofs
      Eigen::MatrixXd Extract_T = Eigen::MatrixXd::Zero(nb_cell_dofs, nb_tot_dofs); // Matrix for the extraction of the cell dofs
      for(int i=0; i< nb_cell_dofs; i++){
        Extract_T(i,i) += 1.;      
      } //for i
      
      //std::vector< Eigen::MatrixXd > Extract_Faces; // vector of extraction matrices for the coordinates of the faces
      //Extract_Faces.resize(T.numberOfFaces()); 
      for(int iF_loc = 0; iF_loc < T.numberOfFaces(); iF_loc++) {        
        int iF = T.faceId(iF_loc);
	      const Face & F = Th->face(iF);
	      const Real & hF = F.measure();
        
        Eigen::MatrixXd Extract_F = Eigen::MatrixXd::Zero(nb_local_face_dofs, nb_tot_dofs); //extraction matrices for the coordinates of the faces
        const int offset_F = nb_cell_dofs + iF_loc * FaceBasis::size;
        for(int i = 0; i < nb_local_face_dofs; i++){
          Extract_F(i, offset_F + i) += 1.; 
        } // for i       
        
        Eigen::MatrixXd J_TF; // matrix repesenting the jump \pi_F^k(u_T) - u_F 
        J_TF =  LU_MFF[iF_loc].solve( MFT[iF_loc] )* Extract_T - Extract_F;
                
        STF += (Lambda_intensity[iF_loc] / hF) * J_TF.transpose() * MFF[iF_loc] * J_TF; // RMK : scaling factor = 1/h_F
      } // for iF_loc

      ATF += STF;

      //------------------------------------------------------------------------------
      // Static condensation      
      
      LU_ATT_omega.compute(ATF.topLeftCorner(nb_cell_dofs, nb_cell_dofs)+ (1./dt) * MTT_omega);

      AF = ATF.bottomRightCorner(nb_tot_faces_dofs, nb_tot_faces_dofs)
        - ATF.bottomLeftCorner(nb_tot_faces_dofs, nb_cell_dofs) * LU_ATT_omega.solve(ATF.topRightCorner(nb_cell_dofs, nb_tot_faces_dofs));

      bF = bTF.tail(nb_tot_faces_dofs) - ATF.bottomLeftCorner(nb_tot_faces_dofs, nb_cell_dofs) * LU_ATT_omega.solve(bTF.head(nb_cell_dofs));

    } // LocalContributions

    //------------------------------------------------------------------------------

    template<std::size_t K>
    Eigen::VectorXd LocalContributions_ExpFitt_time<K>::interpolate(const Mesh * Th,
                                                       const Integer & iT,
                                                       const ExactSolutionType & u) const
    {
      const Cell & T = Th->cell(iT);

      Eigen::VectorXd uTF = Eigen::VectorXd::Zero(nb_cell_dofs + T.numberOfFaces() * FaceBasis::size);
      Eigen::VectorXd bT = Eigen::VectorXd::Zero(nb_cell_dofs);

      for(int iF_loc = 0; iF_loc < T.numberOfFaces(); iF_loc++) {
        const PyramidIntegrator & pim = *pims[iF_loc];

        for(std::size_t i = 0; i < nb_cell_dofs; i++) {
          for(int iQN = 0; iQN < pim.numberOfPoints(); iQN++) {
            const Point & xQN = pim.point(iQN);
            const Real & wQN = pim.weight(iQN);

            const typename CellBasis::ValueType & phi_i_iqn = (*feval_basisT_PTF[iF_loc]) (i, iQN);

            bT(i) += wQN * phi_i_iqn * u(xQN);
          } // for iQN
        } // for i

        const FaceIntegrator & fim = *fims[iF_loc];

        Eigen::VectorXd bF = Eigen::VectorXd::Zero(FaceBasis::size);

        for(std::size_t i = 0; i < FaceBasis::size; i++) {
          for(int iQN = 0; iQN < fim.numberOfPoints(); iQN++) {
            const Point & xQN = fim.point(iQN);
            const Real & wQN = fim.weight(iQN);

            const typename FaceBasis::ValueType & phi_i_iqn = (*feval_basisF[iF_loc]) (i, iQN);

            bF(i) += wQN * phi_i_iqn * u(xQN);
          } // for iQN
        } // for i

        std::size_t offset_F = nb_cell_dofs + iF_loc * FaceBasis::size;
        uTF.segment<FaceBasis::size>(offset_F) = LU_MFF[iF_loc].solve(bF);
      } // for iF_loc

      uTF.head(nb_cell_dofs) = LU_MTT.solve(bT);

      return uTF;
    } // interpolate

    //------------------------------------------------------------------------------

    template<std::size_t K>
    template<class SolutionVectorType, class IndexVectorType>
    Eigen::VectorXd LocalContributions_ExpFitt_time<K>::reconstruct_time(const Mesh * Th,
                                                       const Integer & iT,  
                                                       const Real & dt,
                                                       const Eigen::VectorXd & U_n,
                                                       const SolutionVectorType & X,
                                                       const IndexVectorType & idx_T) const
    {
      Eigen::VectorXd uTF = Eigen::VectorXd::Zero(nb_tot_dofs);
      for(int i = 0; i < idx_T.size(); i++) { 
        uTF(nb_cell_dofs + i) =X(idx_T(i)); 
      } // for i 
      Eigen::VectorXd u_Faces =  uTF.tail(nb_tot_faces_dofs);
      Eigen::VectorXd rhs_recons = bTF.head(nb_cell_dofs) + (1. / dt) * MTT_omega * U_n - ATF.topRightCorner(nb_cell_dofs, nb_tot_faces_dofs) * u_Faces  ;
      uTF.head(nb_cell_dofs) = LU_ATT_omega.solve(rhs_recons);
      return uTF;
    } // reconstruct          
    
    //------------------------------------------------------------------------------

    template<std::size_t K>
    template<class SolutionVectorType, class IndexVectorType>
    Eigen::VectorXd LocalContributions_ExpFitt_time<K>::reconstruct_time_first_step(const Mesh * Th,
                                                       const Integer & iT,  
                                                       const Real & dt,
                                                       const ExactSolutionType & u_0,
                                                       const SolutionVectorType & X,
                                                       const IndexVectorType & idx_T) const
    {
      Eigen::VectorXd uTF = Eigen::VectorXd::Zero(nb_tot_dofs);
      for(int i = 0; i < idx_T.size(); i++) { 
        uTF(nb_cell_dofs + i) =X(idx_T(i)); 
      } // for i 
      Eigen::VectorXd u_Faces =  uTF.tail(nb_tot_faces_dofs);      
      
      //computation of int_T u^0 (the continuous data)
      const Cell & T = Th->cell(iT);  
      const std::size_t N_basis_T = nb_local_cell_dofs;
      Eigen::VectorXd F_0 = Eigen::VectorXd::Zero(nb_local_cell_dofs);        
      
      for(Integer iF_loc = 0; iF_loc < T.numberOfFaces(); iF_loc++) {
        const PyramidIntegrator & pim = *pims[iF_loc];

        for(Integer iQN = 0; iQN < pim.numberOfPoints(); iQN++) {
          const Point & xQN = pim.point(iQN);
          const Real & wQN = pim.weight(iQN);          
          const Real & u_0_iqn = u_0(xQN);

          for(std::size_t i = 0; i < N_basis_T ; i++) {
            const typename CellBasis::ValueType & phi_i_iqn = (*feval_basisT_PTF[iF_loc])(i, iQN);
            F_0[i] +=  wQN *u_0_iqn  * phi_i_iqn  ;
          } // for i         

        } // for iQN
      } // for iF_loc
      Eigen::VectorXd rhs_recons = bTF.head(nb_cell_dofs) + (1. / dt) * F_0 - ATF.topRightCorner(nb_cell_dofs, nb_tot_faces_dofs) * u_Faces  ;
      uTF.head(nb_cell_dofs) = LU_ATT_omega.solve(rhs_recons);
      return uTF;
    } // reconstruct      

//------------------------------------------------------------------------------
    
    template<std::size_t K>
    Eigen::VectorXd LocalContributions_ExpFitt_time<K>::compute_previous_time_contrib(const Mesh * Th, 
                                                  const Real & dt, 
                                                  const Integer & iT,
                                                  const Eigen::VectorXd & rho_T) const
    {
      Eigen::VectorXd F_n = Eigen::VectorXd::Zero(nb_tot_faces_dofs);
      F_n = ATF.bottomLeftCorner(nb_tot_faces_dofs, nb_cell_dofs) * LU_ATT_omega.solve(MTT_omega* rho_T) ;
      return (1. / dt) * F_n;  
    } // compute contrib previous 
    
    //------------------------------------------------------------------------------
    
    template<std::size_t K>
    Eigen::VectorXd LocalContributions_ExpFitt_time<K>::compute_previous_time_contrib_first_step(const Mesh * Th, 
                                                  const Real & dt, 
                                                  const Integer & iT,
                                                  const ExactSolutionType & u_0) const
    {      
      //computation of int_T u^0 (the continuous data)
      const Cell & T = Th->cell(iT);  
      const std::size_t N_basis_T = nb_local_cell_dofs;
      Eigen::VectorXd F_0 = Eigen::VectorXd::Zero(nb_local_cell_dofs);        
      
      for(Integer iF_loc = 0; iF_loc < T.numberOfFaces(); iF_loc++) {
        const PyramidIntegrator & pim = *pims[iF_loc];

        for(Integer iQN = 0; iQN < pim.numberOfPoints(); iQN++) {
          const Point & xQN = pim.point(iQN);
          const Real & wQN = pim.weight(iQN);          
          const Real & u_0_iqn = u_0(xQN);

          for(std::size_t i = 0; i < N_basis_T ; i++) {
            const typename CellBasis::ValueType & phi_i_iqn = (*feval_basisT_PTF[iF_loc])(i, iQN);
            F_0[i] +=  wQN * u_0_iqn  * phi_i_iqn  ;
          } // for i         

        } // for iQN
      } // for iF_loc
      Eigen::VectorXd F_n = Eigen::VectorXd::Zero(nb_tot_faces_dofs);
      F_n = ATF.bottomLeftCorner(nb_tot_faces_dofs, nb_cell_dofs) * LU_ATT_omega.solve(F_0) ;
      return (1. / dt) * F_n;  
    }

//------------------------------------------------------------------------------


    template<std::size_t K>
    template<class SolutionVectorType, class IndexVectorType>
    void LocalContributions_ExpFitt_time<K>::Compute_diff_L2_H1(const Mesh * Th,
                                  const Integer & iT,
                                  const IndexVectorType & idx_T,
                                  const FctContType & omega,
                                  const PotentialType & Phi,
                                  const SolutionVectorType & X_rho,
                                  const ExactSolutionType & u_ex,
                                  const ExactGradientType & Grad_ex,
                                  Real & L2_square,
                                  Real & H1_square,
                                  Real & min_cell,
                                  Real & min_faces  ) const
    {
      const Cell & T = Th->cell(iT);  
      const std::size_t N_basis_T = nb_local_cell_dofs;
      
      const Eigen::VectorXd & rho_TF = reconstruct(Th, iT, X_rho, idx_T);
      const Eigen::VectorXd & rho_T = rho_TF.head(nb_local_cell_dofs);
      
      const Eigen::VectorXd & Phi_TF = interpolate(Th, iT, Phi );
      
      Eigen::VectorXd G_rho_T_coeffs = G_full_T * rho_TF; //computation of the discrete gradient of rho
      Eigen::VectorXd G_Phi_T_coeffs = G_full_T * Phi_TF; //computation of the discrete gradient of the potential   
      //Eigen::VectorXd G_u_I_T_coeffs = G_full_T *interpolate(Th, iT, u_ex); //computation of the discrete gradient of the interpolate of the exact solution   

      for(Integer iF_loc = 0; iF_loc < T.numberOfFaces(); iF_loc++) {
        const PyramidIntegrator & pim = *pims[iF_loc];
        for(Integer iQN = 0; iQN < pim.numberOfPoints(); iQN++) {
          const Point & xQN = pim.point(iQN);
          const Real & wQN = pim.weight(iQN);        
        
          //computation of L^2 norms on functions
          const Real u_ex_iqn = u_ex(xQN); 
          Real u_expfitt_iqn = 0. ; 
          for(std::size_t i = 0; i < N_basis_T ; i++) {
            const typename CellBasis::ValueType & phi_i_iqn = (*feval_basisT_PTF[iF_loc])(i, iQN);
            u_expfitt_iqn += rho_T(i) * omega(xQN)  * phi_i_iqn  ;
          } // for i
          L2_square += wQN * (u_expfitt_iqn - u_ex_iqn) * (u_expfitt_iqn - u_ex_iqn);  
          min_cell = std::min(min_cell, u_expfitt_iqn); 
        
          //computation of the L^2 norms on gradients 
          const Eigen::Matrix<Real, 2, 1> & G_ex_iqn  = Grad_ex(xQN);
          Eigen::Matrix<Real, 2, 1> G_expfitt_iqn = Eigen::Matrix<Real, 2, 1>::Zero();
          //Eigen::Matrix<Real, 2, 1> G_I_iqn = Eigen::Matrix<Real, 2, 1>::Zero();
          for(std::size_t i = 0; i < NG_full; i++) {
            const typename CellVecBasis::ValueType & Tau_i_iqn = (*feval_vec_basisT_PTF[iF_loc])(i, iQN);
            G_expfitt_iqn += ( G_rho_T_coeffs(i) * omega(xQN) - u_expfitt_iqn  * G_Phi_T_coeffs(i) )* Tau_i_iqn ; 
            //G_I_iqn += G_u_I_T_coeffs(i) * Tau_i_iqn;
          } // for i
          H1_square += wQN * (G_ex_iqn - G_expfitt_iqn).dot(G_ex_iqn - G_expfitt_iqn);
          //H1_square += wQN * (G_I_iqn - G_expfitt_iqn).dot(G_I_iqn - G_expfitt_iqn);
        } // for iQN
      } // for iF
    } // Compute_diff_L2_H1      
    
    template<std::size_t K>
    void LocalContributions_ExpFitt_time<K>::Compute_long_time( 
                              const Mesh * Th,
                              const Integer & iT,
                              const FctContType & omega,
                              const Eigen::VectorXd & rho_T,
                              const Real & rho_eq,
                              Real & L2_square,
                              Real & L1_square,
                              Real & min_cell,
                              int & nb_neg_p ,
                              int & nb_neg_c ) const
    {
      const Cell & T = Th->cell(iT);  
      const std::size_t N_basis_T = nb_local_cell_dofs;        
      Real mass_T =0 ; //mass of the solution on the cell T

      for(Integer iF_loc = 0; iF_loc < T.numberOfFaces(); iF_loc++) {        
      
        const PyramidIntegrator & pim = *pims[iF_loc]; 
        Real mass_TF =0 ; //mass of the solution on the pyramid P_{TF}

        for(Integer iQN = 0; iQN < pim.numberOfPoints(); iQN++) {
          const Point & xQN = pim.point(iQN);
          const Real & wQN = pim.weight(iQN);        
        
          //computation of L^2 norms on functions
          const Real u_eq_iqn = rho_eq * omega(xQN); 
          Real u_expfitt_iqn = 0. ; 
          for(std::size_t i = 0; i < N_basis_T ; i++) {        
            const typename CellBasis::ValueType & phi_i_iqn = (*feval_basisT_PTF[iF_loc])(i, iQN);       
            u_expfitt_iqn += rho_T(i) * omega(xQN)  * phi_i_iqn  ;            
          } // for i            
          L2_square += wQN * (u_expfitt_iqn - u_eq_iqn) * (u_expfitt_iqn - u_eq_iqn);            
          L1_square += wQN * std::fabs(u_expfitt_iqn - u_eq_iqn) ;           
          mass_TF +=  wQN * u_expfitt_iqn ; 
          min_cell = std::min(min_cell, u_expfitt_iqn);                   
        } // for iQN
        if(mass_TF < 0){nb_neg_p += 1;}
        mass_T += mass_TF;
      } // for iF
      if(mass_T < 0){nb_neg_c += 1;}
    } //compute long_time    
    
    
    
    
    template<std::size_t K>    
    template<class SolutionVectorType, class IndexVectorType>
    void LocalContributions_ExpFitt_time<K>::Compute_mins(  const Mesh * Th,
                        const Integer & iT,
                        const FctContType & omega,
                        const Eigen::VectorXd & rho_T,
                        const SolutionVectorType & X,
                        const IndexVectorType & idx_T,
                        Real & min_cell_mean,
                        Real & min_faces_mean,
                        Real & min_QN_cells,
                        Real & min_QN_faces,                    
                        int & nb_neg_cells_mean) const
    {
      const Cell & T = Th->cell(iT);  
      const std::size_t N_basis_T = nb_local_cell_dofs;  

      Eigen::VectorXd rho_TF = Eigen::VectorXd::Zero(nb_tot_dofs);
      for(int i = 0; i < idx_T.size(); i++) { 
        rho_TF(nb_cell_dofs + i) =X(idx_T(i)); 
      } // for i 
      //Eigen::VectorXd u_Faces =  uTF.tail(nb_tot_faces_dofs); 

      Real integral_T =0 ; //integral of the solution on the cell T

      for(Integer iF_loc = 0; iF_loc < T.numberOfFaces(); iF_loc++) {        
      
        // computation int_PKF u
        const PyramidIntegrator & pim = *pims[iF_loc]; 
        Real integral_TF =0 ; //mass of the solution on the pyramid P_{TF}

        for(Integer iQN = 0; iQN < pim.numberOfPoints(); iQN++) {
          const Point & xQN = pim.point(iQN);
          const Real & wQN = pim.weight(iQN);        
        
          //computation of L^2 norms on functions
          Real u_expfitt_iqn = 0. ; 
          for(std::size_t i = 0; i < N_basis_T ; i++) {        
            const typename CellBasis::ValueType & phi_i_iqn = (*feval_basisT_PTF[iF_loc])(i, iQN);       
            u_expfitt_iqn += rho_T(i) * omega(xQN)  * phi_i_iqn  ;            
          } // for i                   
          integral_TF +=  wQN * u_expfitt_iqn ; 
          min_QN_cells = std::min(min_QN_cells, u_expfitt_iqn);                   
        } // for iQN
        integral_T += integral_TF;
        
        // computation int_F u          
        // Offset for face unknowns
        const std::size_t offset_F = nb_cell_dofs + iF_loc * FaceBasis::size; 
        Real integral_F = 0.; 
        int iF = T.faceId(iF_loc);											
        const Face & F = Th->face(iF);																	
        const Real & hF = F.measure();			       								
        const FaceIntegrator & fim = *fims[iF_loc];        
        
        
          for(int iQN = 0; iQN < fim.numberOfPoints(); iQN++) {
            const Point & xQN = fim.point(iQN);
            const Real & wQN = fim.weight(iQN);
            
            Real u_expfitt_iqn = 0. ; 
            for(std::size_t i = 0; i < FaceBasis::size; i++) {
                const typename FaceBasis::ValueType & phi_i_iqn = (*feval_basisF[iF_loc]) (i, iQN);
                u_expfitt_iqn += rho_TF(offset_F + i) * omega(xQN)  * phi_i_iqn  ;
            } // for i

            integral_F +=  wQN * u_expfitt_iqn ; 
            min_QN_faces = std::min(min_QN_faces, u_expfitt_iqn);   
          } // for iQN      
          integral_F = integral_F / hF; 
          min_faces_mean = std::min(min_faces_mean , integral_F); 

      } // for iF
      if(integral_T < 0){nb_neg_cells_mean+= 1;}
      integral_T = integral_T / T.measure(); 
      min_cell_mean = std::min(min_cell_mean , integral_T); 
    } //compute long_time

  } // namespace pho

} // namespace ho




#endif
