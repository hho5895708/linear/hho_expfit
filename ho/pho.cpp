#include "pho.h"

#include <Common/GetPot>

#include <boost/integer/static_min_max.hpp>
#include <boost/math/constants/constants.hpp>

#include "Common/chrono.hpp"

#include "Data.h"
#include "postProcessing.h"

#include "Eigen/SparseCore"
#include "Eigen/SparseLU"

#include <unsupported/Eigen/SparseExtra>

#ifndef DEGREE
#define DEGREE 1
#endif

#ifndef nb_pts_in_graph
#define nb_pts_in_graph 12
#endif


using boost::math::constants::pi;

using namespace ho;

//typedef ho::pho::LocalContributions<DEGREE> LC;

//------------------------------------------------------------------------------

typedef Eigen::Matrix<std::size_t, Eigen::Dynamic, 1> IndexVectorType;
typedef Eigen::SparseMatrix<Real> SparseMatrixType;
typedef Eigen::Matrix<Real, Eigen::Dynamic, 1> SolutionVectorType;
typedef Eigen::Triplet<Real> TripletType;

typedef ho::pho::LocalContributions_ExpFitt_time<DEGREE> LC;  
typedef ho::HierarchicalScalarBasis1d<DEGREE> FaceBasis;


//------------------------------------------------------------------------------
// Functions, declarations
IndexVectorType face_unknown_local_idx( const Mesh * Th, const Integer & iT); 



std::shared_ptr<Mesh> Mesh_read(const string & Mesh_file);

std::vector<Real> hho_evol(const Mesh *Th, const Real & T_end , const Real & time_step , const bool & Visu);

int main_visu();
//int main_convergence();

//vector<Real>  computations_test_loc(const Mesh *Th, const int & iT);

//------------------------------------------------------------------------------
// Functions, implementation 

// int main()
// { 
//   test_hho();
//   return 1.;
// }
int main(){

  std::cout << "\n\nScheme considered : based on the exponential fitting strategy:" << std::endl; 
  std::cout << "\n Mixed HHO("<< DEGREE <<","<< DEGREE+1 <<") version, with Lehrenfeld-Schöberl stabilisation and full gradient \n"  << std::endl;
  std::cout << "Please double check that you use the correct degree unknow."  << std::endl;
  std::cout << "Reminder: the expected accuracy order in L^2 norm is " <<  DEGREE+2 << std::endl;

  std::cout << "\n\n ~~~~~~  ~~~~~~  ~~~~~~  ~~~~~~  ~~~~~~  ~~~~~~  ~~~~~~  ~~~~~~  ~~~~~~   \n\n"  << std::endl;    
  
  main_visu();
  return 0;

  // std::string choice; 
  // std::cout << "Select your mode by giving a keyword: "  << std::endl;
  // std::cout << " - convergence study ->  convergence "  << std::endl;
  // std::cout << " - visualisation and positivity -> visu "  << std::endl;
  // cin >> choice ; 
  // std::cout << " \n"  << std::endl;

  // if(choice == "convergence"){

  //   main_convergence();
  //   return 0;
  // } //if convergence  
  
  // else if(choice == "visu"){

  //   main_visu();
  //   return 0;
  // } //if convergence  
  
  // else{
  //   std::cout << "The keyword does not correspond to any implemented mode !!!"  << std::endl;
  //   std::cout << "End of the simulation."  << std::endl;
  //   return 0;
  // } //if convergence
  
}


int main_visu(){
    
    bool visu = 0; 
    cout << "Visualisation and positivity test..." << endl;
    cout << "You will have to select a mesh, a final time and a time step" << endl;    
        
        
    cout << "Do you want to visualise the computed solution ? (1 for yes, 0 for no)" << endl;
    cin >> visu ;

    string  geo_mesh = "pi6_tiltedhexagonal_5"; // = "hexagonal";
    //string  geo_mesh = "mesh_quad_4";
    //int  i_max ; //= 5;

    Real T; 
    Real Dt; 
    // Real T = 5.*std::pow(10.,-4); //= 0.1;
    // Real Dt = T / 224. ; 
    cout << "Mesh ? (name with number, ex : hexagonal_3 or mesh3_5)" << endl;
    cin >> geo_mesh ;

    cout << "Final time ? " << endl;
    cin >> T;

    cout << "Time step? " << endl;
    cin >> Dt;

    ho::pho::Regular sol;

    std::string Mesh_base = "../meshes/";
    std::string Mesh_file = Mesh_base + geo_mesh +".typ1";
    const std::shared_ptr<Mesh> Th = Mesh_read(Mesh_file);

    std::vector<Real> Data_errors;    
    Data_errors = hho_evol(Th.get(), T, Dt, visu);
    
    //cout << "Regarding the errors, please double check in the data file that you are using a test case with explicit exact solution !! \n" << endl;

    std::cout << "Done" << std::endl;
    return 0;
} // main_visu


// int main_convergence()
// { 
  
//     cout << "Convergence test ..." << endl;
//     cout << "You will have to select a mesh family, and a number of refinement" << endl;    
//     cout << "Please double check in the data file that you are using a test case with explicit exact solution !! \n" << endl;
//     string  geo_mesh ; // = "hexagonal";
//     int  i_max ; //= 5;

//     Real T = 1.;
//     Real Dt = 0.01;
//     bool Visu = false; 

//     cout << "Mesh geometry ? (name without _, ex : hexagonal or mesh3)" << endl;
//     cin >> geo_mesh ;

//     cout << "Number of meshes ? " << endl;
//     cin >> i_max; 

//     std::string Mesh_base = "../meshes/";
//     std::vector<vector<Real>> Data_errors;
//     Data_errors.resize(i_max);    
    
//     for(int i=1; i< i_max+1 ; i++){
//         std::string Mesh_file = Mesh_base + geo_mesh +"_" + std::to_string(i) +".typ1";
//         const std::shared_ptr<Mesh> Th = Mesh_read(Mesh_file);
//         Data_errors[i-1] = hho_evol(Th.get(), T, Dt, Visu);
//     } //for i 
//     std::cout << "Computation of errors : OK  \n" << std::endl;
//     std::cout << " ~~~~~~~~~~~~~~~~~~~~~ \n" << std::endl;
//     std::cout << "Order u (L2) : \t" ;
//     for(int j = 1; j< i_max; j++){
//         std::cout << log(Data_errors[j][1]/Data_errors[j-1][1]) / log(Data_errors[j][0]/Data_errors[j-1][0]) << "\t";
//     } // for j 

//     std::cout << "\n  " << std::endl ;
//     std::cout << "Order gradient (L2) : " ;
//     for(int j = 1; j< i_max; j++){
//         std::cout << log(Data_errors[j][2]/Data_errors[j-1][2]) / log(Data_errors[j][0]/Data_errors[j-1][0]) << "\t";
//     } // for j 

//     std::ofstream file_graph;
//     file_graph.open ("errors_graph_hho_expfitt_"+ std::to_string(DEGREE) + ".txt");
//     file_graph << "meshsize" << " " << "errl2" << " " << "errh1" << " " << "min_cell" << " " << "min_edge"  << endl;
//     for(int j = 0; j< i_max; j++){
//       file_graph << Data_errors[j][0] <<" "<< Data_errors[j][1] 
//           <<" "<< Data_errors[j][2] <<" "<< Data_errors[j][3] <<" "<< Data_errors[j][4]<< endl;
//     }//for j
//     file_graph.close();
    
//     cout << "\nRegarding the errors, please double check in the data file that you are using a test case with explicit exact solution !! \n" << endl;

//     std::cout << "Done" << std::endl;
//     return 0;
// }

std::shared_ptr<Mesh> Mesh_read(const string & Mesh_file){

    std::string sep = "\n----------------------------------------\n";
    Eigen::IOFormat CleanFmt(4, 0, ", ", "\n", "[", "]");
    std::cout << "Reading the mesh "  << std::endl;

    // Read options and data
    //GetPot options(argc, argv);
    std::string mesh_file = Mesh_file;

    // Pretty-print floating point numbers
    std::cout.precision(2);
    std::cout.flags(std::ios_base::scientific);

    std::cout << "Mesh file name : " << Mesh_file << std::endl;
    // Create mesh
    std::shared_ptr<Mesh> Th(new Mesh());
    if(mesh_file.find("typ1")!=std::string::npos)
        Th->readFVCA5File(mesh_file.c_str());
    else if(mesh_file.find("dgm")!=std::string::npos)
        Th->readDGMFile(mesh_file.c_str());
    else {
        std::cerr << "Unknown mesh format" << std::endl;
        exit(1);
    }

    std::cout << "Mesh created"  << std::endl;

    Th->buildFaceGroups();
    std::cout << "MESH: " << mesh_file << std::endl;
    std::cout << FORMAT(50) << "num_cells" << Th->numberOfCells() << std::endl;
    std::cout << FORMAT(50) << "num_faces" << Th->numberOfFaces() << std::endl;
    std::cout << FORMAT(50) << "num_unknws_cells" << Th->numberOfCells() * LC::nb_cell_dofs << std::endl;
    std::cout << FORMAT(50) << "num_unknws_faces" << Th->numberOfInternalFaces() * LC::nb_local_face_dofs + Th->numberOfBoundaryFaces() * LC::nb_local_face_dofs << std::endl;
    
    //------------------------------------------------------------------------------
    // Estimate mesh size
    Real h = Th->meshsize();
    std::cout << FORMAT(50) << "meshsize" << h << std::endl;

    return Th;
}



std::vector<Real> hho_evol(const Mesh *Th, const Real & T_end , const Real & time_step , const bool & Visu)
{ 
  
  std::string sep = "\n----------------------------------------\n";
  Eigen::IOFormat CleanFmt(4, 0, ", ", "\n", "[", "]");
  std::cout << "\n Mixed HHO("<< DEGREE <<","<< DEGREE+1 <<") version, with Lehrenfeld-Schöberl stabilisation and full gradient"  << std::endl;
  std::cout << "Non stationnary problem solved with exponential fitting" << std::endl;
  std::cout << "Wanted time step : \t" << time_step << std::endl;

  const int N = std::ceil(T_end / time_step);
  const Real & dt = T_end / N;
  std::cout << "Effective time step : \t" << time_step << std::endl;
  
  const Real & h = Th->meshsize();
   

  //------------------------------------------------------------------------------
  // Exact solution 

  ho::pho::Regular sol;
  DefBoundaryConditionType isNeu = sol.isNeumann;
  DefBoundaryConditionType isDir = sol.isDirichlet;


  //------------------------------------------------------------------------------
  // Exponential fitting data 

  const FctContType omega = [sol](const Point & x) -> Real {
    Real arg = - sol.Phi(x);
    return std::exp(arg);
  };      

  const DiffusivityType Lambda_omega = [sol, omega](const Point & x) -> TensorType {
    return omega(x) * sol.Lambda(x);
  };  
  
  const BoundaryDirichletType rho_D = [sol, omega](const Point & x) -> Real {
    return sol.u_b(x) /  omega(x);
  };  
  
  const InitialDataType rho_0 = [sol, omega](const Point & x) -> Real {
    return sol.u_0(x) /  omega(x);
  };

  //------------------------------------------------------------------------------
  // Global numbering for boundary faces


  std::vector<Integer> boundary_face_global_id(Th->numberOfFaces());
  std::fill(boundary_face_global_id.begin(), boundary_face_global_id.end(), -1);
  std::size_t boundary_face_id = 0;
  int nb_Dirichlet_faces = 0;
  for(Integer iF = 0; iF < Th->numberOfFaces(); iF++) {
    const Face & F = Th->face(iF);
    if(isDir(F)){ 
      boundary_face_global_id[iF] = boundary_face_id++; 
      nb_Dirichlet_faces += 1; 
      } // if F is Dirichlet
  } // for iF 
  
  std::cout << "Number of faces with Dirichlet boundary condition " << nb_Dirichlet_faces << std::endl;
  std::cout << "Number of unknowns associated to Dirichlet boundary condition " << nb_Dirichlet_faces * LC::nb_local_face_dofs << std::endl;
  
  //------------------------------------------------------------------------------
  // Precompute local contributions
  std::cout << "\n Precomputations begin"  << std::endl;  
  common::chrono c_comp;
  c_comp.start();
  
  std::vector<LC> Local_Contribs; // vectors of local contributions
  for(int iT = 0; iT < Th->numberOfCells(); iT ++){
    const LC  lc(Th, iT, omega, dt,  sol.f, sol.g_n, Lambda_omega , isNeu );
    Local_Contribs.push_back( lc );
  }
  
  c_comp.stop();
  std::cout << "Precomputations : DONE !"  << std::endl;  
  std::cout << FORMAT(50) << "Time of the precomputational step"  << c_comp.diff() << std::endl;
  
  
  //------------------------------------------------------------------------------
  // Assemble matrix
  
  std::cout << "Assembly of the matrices begins"  << std::endl;  

  int nunkw =
    (Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces()) * LC::nb_local_face_dofs;
  int nuntrp =
    (Th->maximumNumberOfFaces() * Th->numberOfCells() + Th->numberOfBoundaryFaces()) * LC::nb_local_face_dofs;

  SparseMatrixType A(nunkw, nunkw);
  std::vector<TripletType> triplets;
  triplets.reserve(nuntrp);

  SolutionVectorType X_D = SolutionVectorType::Zero(nunkw); //pseudo lifting of the Dirichlet boundary 
  SolutionVectorType X_0 = SolutionVectorType::Zero(nunkw); //interpolate of the initial data (in rho) on faces
  std::vector<Eigen::VectorXd> rho_0_cells; // interpolate of the initial data on cells
  rho_0_cells.resize(Th->numberOfCells()); 
  SolutionVectorType B_sta = SolutionVectorType::Zero(nunkw);

  SparseMatrixType resize_mat(nunkw - nb_Dirichlet_faces * LC::nb_local_face_dofs, nunkw);
  std::vector<TripletType> triplets_resize;
  triplets_resize.reserve(nunkw);

  common::chrono c_assembly;
  c_assembly.start();
  for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
    const LC & lc = Local_Contribs[iT];
    IndexVectorType idx_T = face_unknown_local_idx(Th, iT);

    const Eigen::VectorXd rho_0_TF = lc.interpolate(Th, iT , rho_0); 
    rho_0_cells[iT] = rho_0_TF.head(LC::nb_cell_dofs);

    //BCs
    Eigen::VectorXd g_Dirichlet_iF = Eigen::VectorXd::Zero(lc.nb_tot_faces_dofs);
    for(int iF_loc = 0; iF_loc < (Th->cell(iT)).numberOfFaces(); iF_loc++){
      if(isDir(Th->face((Th->cell(iT)).faceId(iF_loc)))){
        g_Dirichlet_iF.segment(iF_loc * LC::nb_local_face_dofs, LC::nb_local_face_dofs) += lc.interpolate(Th, iT, rho_D ).segment(LC::nb_cell_dofs + iF_loc * LC::nb_local_face_dofs, LC::nb_local_face_dofs);
      } // if Face is dirichlet
    }
    Eigen::VectorXd ATF_bc = lc.AF * g_Dirichlet_iF;

    for(int i = 0; i < idx_T.size(); i++) {
      B_sta(idx_T(i)) += - ATF_bc(i) + lc.bF(i);
      X_D(idx_T(i)) += g_Dirichlet_iF(i);
      X_0(idx_T(i)) = (rho_0_TF.tail(lc.nb_tot_faces_dofs))(i); // verifier en cas de probleme 
      for(int j = 0; j < idx_T.size(); j++) {
        triplets.push_back(TripletType(idx_T(i), idx_T(j), lc.AF(i, j)));
      } // for i
    } // for j
  } // for iT

  int counter_Fb_dir = 0;
  for(Integer iF = 0; iF < Th->numberOfFaces(); iF++){
    const Face F = Th->face(iF);
    if(!(isDir(F))){
      for(int i = 0; i < LC::nb_local_face_dofs; i++){
        triplets_resize.push_back(TripletType((iF - counter_Fb_dir) * LC::nb_local_face_dofs + i, iF * LC::nb_local_face_dofs + i, 1.));
      }// for i
    }//if Face is not Dirichlet 
    else{counter_Fb_dir++;}
    
    
  } // for iF 

  A.setFromTriplets(triplets.begin(), triplets.end());
  resize_mat.setFromTriplets(triplets_resize.begin(), triplets_resize.end());
  SparseMatrixType GLOBAL_LHS = resize_mat * A * (resize_mat.transpose());
  //Eigen::VectorXd GLOBAL_RHS = resize_mat * B;
  c_assembly.stop();  
  std::cout << "Assembly : DONE !"  << std::endl; 

  
  //std::cout << " \n resize mat  \n " << std::endl;
  //std::cout << resize_mat << std::endl;  

  // std::cout << " \n \n X_D \n " << std::endl;
  // std::cout << X_D  << std::endl;  
  
  // std::cout << "\n \n RHS \n " << std::endl;
  // std::cout << GLOBAL_RHS  << std::endl;

  //------------------------------------------------------------------------------
  // 
  std::vector<Real> Time;
  std::vector<SolutionVectorType> Sol_rho_faces; //vector containing the X^n (coefficients of the rho_F, global version)
  std::vector<std::vector<Eigen::VectorXd>> Sol_rho_cells; // vector containing the coefficient of the (rho_T)_(T \in \Th)
  
  Time.push_back(0.); 
  Sol_rho_faces.push_back(X_0);
  Sol_rho_cells.push_back(rho_0_cells);

  //------------------------------------------------------------------------------
  // Solve linear system
  std::cout << "Factorisation of the global matrix (time dependent problem) begins"  << std::endl; 

  common::chrono c_facto;
  c_facto.start();
  Eigen::SparseLU<SparseMatrixType> solver_evo;
  // Compute the ordering permutation vector from the structural pattern of A
  solver_evo.analyzePattern(GLOBAL_LHS);
  // Compute the numerical factorization
  solver_evo.factorize(GLOBAL_LHS);
  c_facto.stop();
  
  std::cout << "Factorisation of the global matrix : DONE !"  << std::endl; 

  //------------------------------------------------------------------------------
  // Reconstruct high-order potential

  // std::vector<Eigen::VectorXd> coeffs_rho(Th->numberOfCells());
  // common::chrono c_recons;
  // c_recons.start();
  // for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
  //   const LC & lc = Local_Contribs[iT];
  //   IndexVectorType idx_T = face_unknown_local_idx(Th, iT);
  //   // Reconstruct local discrete solution
  //   Eigen::VectorXd rho_TF = lc.reconstruct(Th, iT, X, idx_T);
  //   coeffs_rho[iT] = rho_TF.head(LC::nb_cell_dofs);
  // } // for iT

  // c_recons.stop();
  int Nb_resol_lin = 0;
    
  common::chrono c_evol;
  c_evol.start();
  Real t = dt ;

  //computation of the solution at first time (with the continuous )
  SolutionVectorType X_1 = SolutionVectorType::Zero(nunkw);
  std::vector<Eigen::VectorXd> rho_cells_1;
  rho_cells_1.resize(Th->numberOfCells()); 
  
  Eigen::VectorXd B_1 = SolutionVectorType::Zero(nunkw);
  B_1 += B_sta; 
        
  // computation of the RHS 
  for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
    const LC & lc = Local_Contribs[iT];
    IndexVectorType idx_T = face_unknown_local_idx(Th, iT);
    const Eigen::VectorXd G_1 = lc.compute_previous_time_contrib_first_step(Th, dt, iT, sol.u_0);
    //std::cout << "G_n \n" << G_n << std::endl; 
    for(int i = 0; i < idx_T.size(); i++) {
      B_1(idx_T(i)) += - G_1(i);
    } // for i
  } // for iT

    Eigen::VectorXd GLOBAL_RHS_1 = resize_mat * B_1;
    X_1 = (solver_evo.solve(GLOBAL_RHS_1)) + X_D;
    //std::cout << "RHS \n" << B_n - B_sta << std::endl;

     
    Sol_rho_faces.push_back(X_1); 
    //std::cout << "X - X^0 \n" << Sol_rho_faces[n+1]- Sol_rho_faces[n]<< std::endl;   

    //reconstruction of the the (rho_T) 
    for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
      const LC & lc = Local_Contribs[iT];
      IndexVectorType idx_T = face_unknown_local_idx(Th, iT);
      rho_cells_1[iT] = Eigen::VectorXd::Zero(lc.nb_cell_dofs);
      rho_cells_1[iT] = lc.reconstruct_time_first_step( Th, iT, dt, sol.u_0, X_1, idx_T).head(LC::nb_cell_dofs);
      //std::cout << "rho_T - X^0 \n" <<  rho_cells[iT] - Sol_rho_cells[n][iT] << std::endl;  
    } // for iT 

    Sol_rho_cells.push_back(rho_cells_1);

    Time.push_back(t);
    t+=dt;



  for(int n = 1; n< N; n++ ){
    if(n%200==0){std::cout << "Computation of the "<< n+1 << "th iteration" << std::endl; }
    
    SolutionVectorType X = SolutionVectorType::Zero(nunkw);
    std::vector<Eigen::VectorXd> rho_cells;
    rho_cells.resize(Th->numberOfCells()); 

    std::vector<Eigen::VectorXd> & rho_n_cells = Sol_rho_cells[n];
    Eigen::VectorXd B_n = SolutionVectorType::Zero(nunkw); 
    B_n += B_sta; 
        
    // computation of the RHS 
    for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
      const LC & lc = Local_Contribs[iT];
      IndexVectorType idx_T = face_unknown_local_idx(Th, iT);
      const Eigen::VectorXd & rho_n_T = rho_n_cells[iT]; 
      const Eigen::VectorXd G_n = lc.compute_previous_time_contrib(Th, dt, iT, rho_n_T);
      //std::cout << "G_n \n" << G_n << std::endl; 
      for(int i = 0; i < idx_T.size(); i++) {
        B_n(idx_T(i)) += - G_n(i);
      } // for i
    } // for iT

    Eigen::VectorXd GLOBAL_RHS_n = resize_mat * B_n;
    X = (solver_evo.solve(GLOBAL_RHS_n)) + X_D;
    //std::cout << "RHS \n" << B_n - B_sta << std::endl;

     
    Sol_rho_faces.push_back(X); 
    //std::cout << "X - X^0 \n" << Sol_rho_faces[n+1]- Sol_rho_faces[n]<< std::endl;   

    //reconstruction of the the (rho_T) 
    for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
      const LC & lc = Local_Contribs[iT];
      IndexVectorType idx_T = face_unknown_local_idx(Th, iT);
      rho_cells[iT] = Eigen::VectorXd::Zero(lc.nb_cell_dofs);
      const Eigen::VectorXd & rho_n_T = rho_n_cells[iT]; 
      rho_cells[iT] = lc.reconstruct_time( Th, iT, dt, rho_n_T, X, idx_T).head(LC::nb_cell_dofs);
      //std::cout << "rho_T - X^0 \n" <<  rho_cells[iT] - Sol_rho_cells[n][iT] << std::endl;  
    } // for iT 

    Sol_rho_cells.push_back(rho_cells);

    Time.push_back(t);
    t+=dt;
    }

  c_evol.stop();
  std::cout << FORMAT(50) << "Computation of the evolutive solution : DONE "  << std::endl;
  std::cout << FORMAT(50) << "Time for the computation of the evolutive solution : "  << c_evol.diff() << std::endl;

  std::cout << FORMAT(50) << "Computation of the thermal equilibrium (BC compatible with the thermal equilibrium)"  << std::endl;  
  
  Real mass_omega =0.;
  Real mass_u_0 =0.; 
  for(int iT =0; iT < Th->numberOfCells(); iT++){
    const Cell & T = Th->cell(iT);
    const LC & lc = Local_Contribs[iT];

    for(int iF_loc =0; iF_loc <T.numberOfFaces(); iF_loc ++ ){
      const PyramidIntegrator & pim = *lc.pims[iF_loc];
      for(Integer iQN = 0; iQN < pim.numberOfPoints(); iQN++) {
        const Point & xQN = pim.point(iQN);
        const Real & wQN = pim.weight(iQN);
        mass_omega += wQN * omega(xQN); 
        mass_u_0  += wQN * sol.u_0(xQN);
      } // for iQN
    } // for iF_loc
  } //for iT  
  
  const Real rho_eq = mass_u_0 / mass_omega ; 

  std::cout << FORMAT(50) << "Computation of the evolutive equlibrium : DONE "  << std::endl; 
  


  //------------------------------------------------------------------------------
  // Print timings
  
  std::cout << FORMAT(50) << "time_precomputation"  << c_comp.diff() << std::endl;
  std::cout << FORMAT(50) << "time_assembly"  << c_assembly.diff() << std::endl;
  std::cout << FORMAT(50) << "time_facto" << c_facto.diff() << std::endl;
  std::cout << FORMAT(50) << "time_evol" << c_evol.diff() << std::endl;
  std::cout << FORMAT(50) << "time_ratio " << (c_comp.diff() + c_assembly.diff() ) / ( c_evol.diff() + c_facto.diff()  ) << std::endl;
  std::cout << FORMAT(50) << "time_tot" <<  c_comp.diff()+ c_assembly.diff() + c_facto.diff()+ c_evol.diff() << std::endl; 
  Real t_tot = c_comp.diff()+ c_assembly.diff() + c_facto.diff()+ c_evol.diff() ;
  
  //------------------------------------------------------------------------------
  // Long-time behaviour
  std::cout << "\n\nComputation of entropy and study of the long-time behaviour" << std::endl;  
  common::chrono c_longtime;
  c_longtime.start();
  
  Point x_ref;
  x_ref(0) = 0.5;
  x_ref(1) = 0.5;

  Real min_cells_mean =  1000.;
  Real min_faces_mean =  1000.;  
  Real min_QN_cells =  1000.;
  Real min_QN_faces =  1000.;
  int nb_neg_pyrs = 0;
  int nb_neg_cells = 0;  
  
  for(int n = 1; n < N+1 ; n ++){

    const SolutionVectorType X_n = Sol_rho_faces[n]; 
    const std::vector<Eigen::VectorXd> & rho_n_cells =  Sol_rho_cells[n];

    for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
      //auto begin = std::chrono::high_resolution_clock::now();
      const LC & lc = Local_Contribs[iT]; 
      IndexVectorType idx_T = face_unknown_local_idx(Th, iT);           
      //auto end = std::chrono::high_resolution_clock::now();
      //auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
      //std::cout << "LC access : \t" << elapsed.count() <<std::endl;   
      const Eigen::VectorXd & rho_n_T = rho_n_cells[iT];
      lc.Compute_mins(Th, iT,  omega,  rho_n_T, X_n, idx_T, min_cells_mean, min_faces_mean,  min_QN_cells, min_QN_faces,  nb_neg_cells);
    } // for iT

  } // for n
  
  std::vector<Real> Diff_L2_square;
  std::vector<Real> Diff_L1_square;  
  
  Real min_1 =  1000.;
  int nb_neg_1 = 0;
  int nb_neg_2 = 0;  
 
  for(int n = 0; n < N+1 ; n ++){
    Real diff_l2 = 0.;
    Real diff_l1 = 0.;

    const std::vector<Eigen::VectorXd> & rho_n_cells =  Sol_rho_cells[n];

    for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
      //auto begin = std::chrono::high_resolution_clock::now();
      const LC & lc = Local_Contribs[iT];            
      //auto end = std::chrono::high_resolution_clock::now();
      //auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
      //std::cout << "LC access : \t" << elapsed.count() <<std::endl;   
      const Eigen::VectorXd & rho_n_T = rho_n_cells[iT];
      lc.Compute_long_time( Th, iT, omega, rho_n_T, rho_eq, diff_l2, diff_l1, min_1,nb_neg_1, nb_neg_2 );
    } // for iT

    // if(n == 0 ){
    //   nb_neg_pyrs = 0;
    //   nb_neg_cells = 0;
    //   min_cells =  sol.u(dt,x_ref) + 5.;
    // } //if n == 0, do not take into account the initial time for the positivity 

    Diff_L2_square.push_back(diff_l2);
    Diff_L1_square.push_back(diff_l1*diff_l1);

  } // for n

  std::cout << "Computation of entropy : DONE"  << std::endl;
  
  const int mod =round(N  / nb_pts_in_graph) +1;

  std::ofstream myfile;
  myfile.open ("temps_long_hh0" + std::to_string(DEGREE) + ".csv");
  myfile << "Première cellule premiere colonne .\n";
  myfile <<"Description "<<"," <<"Convection diffusion, ExpFitt hho"+ std::to_string(DEGREE) +"  .\n";
  myfile <<"Pas de temps"<<"," << dt  << endl;
  myfile <<"Nombre d'itérations "<<"," << N  << endl;
  myfile <<"Finesse de maillage"<<"," << h  << endl;
  myfile <<"" << endl;
  myfile << "Temps,Diff_L1_square,Diff_L2_square,\n";
  for(int n=0; n< N + 1 ; n ++){
    myfile << Time[n] <<","<<Diff_L1_square[n] <<"," << Diff_L2_square[n]   << endl;
  }
  myfile.close();

  std::ofstream myfile2;
  myfile2.open ("time_hho" + std::to_string(DEGREE));
  myfile2 << "Temps Diff_L1_square Diff_L2_square Diff_L1 Diff_L2 \n";
  for(int n=0; n< N +1; n ++){
    if(n % mod ==0){
      myfile2 << Time[n] <<" "<<Diff_L1_square[n] <<" " << Diff_L2_square[n]  << " "<< std::sqrt(Diff_L1_square[n]) <<" " << std::sqrt(Diff_L2_square[n]) << endl;
     }// if n % 100 ==0
  }//for n
  myfile2.close();
  
  c_longtime.stop();
  std::cout <<  "Generation of the 'long-time behaviour' file : DONE" << std::endl;
  std::cout <<  "Time for the computation : \t" << c_longtime.diff() << std::endl;
  //------------------------------------------------------------------------------
  // Visualisation of the solution 
  

  
  if(Visu){
    std::cout << "\n\nGeneration of the visualisation files begins" << std::endl;
  
    // for(int i=0; i< N+1; i++ ){
    //   if(i % mod ==0){
    //     const std::vector<Eigen::VectorXd> & coeffs_rho = Sol_rho_cells[i];
    //     // for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
    //     //   Eigen::VectorXd uh_F_end (1);
    //     //   uh_F_end(0) =Sol_maille[i](iT);
    //     //    coeffs_u[iT] = uh_F_end;
    //     // } // for iT

    //     char str[20] = {0};
    //     std::sprintf(str, "%d", i);
    //     char name_c[40];
    //     strcpy(name_c,"output/uh_");
    //     strcat(name_c, str);
    //     strcat(name_c,".vtu");
    //     postProcessing_expfitt<ho::HierarchicalScalarBasis2d<DEGREE+1>, boost::static_unsigned_min<DEGREE+1,3>::value>(name_c, Th, omega, coeffs_rho);

    //   } // if 


    // } //for i

      for(int i=0; i< 6; i++ ){
        const std::vector<Eigen::VectorXd> & coeffs_rho = Sol_rho_cells[i];
        // for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
        //   Eigen::VectorXd uh_F_end (1);
        //   uh_F_end(0) =Sol_maille[i](iT);
        //    coeffs_u[iT] = uh_F_end;
        // } // for iT

        char str[20] = {0};
        std::sprintf(str, "%d", i);
        char name_c[40];
        strcpy(name_c,"output/uh_");
        strcat(name_c, str);
        strcat(name_c,".vtu");
        postProcessing_expfitt<ho::HierarchicalScalarBasis2d<DEGREE+1>, boost::static_unsigned_min<DEGREE+1,3>::value>(name_c, Th, omega, coeffs_rho);
      } //for i

    postProcessing_expfitt<ho::HierarchicalScalarBasis2d<DEGREE+1>, boost::static_unsigned_min<DEGREE+1,3>::value>("output/uh_end.vtu", Th, omega, Sol_rho_cells[N]);
    std::cout << "Generation of the visualisation files : DONE" << std::endl;
  }

  //------------------------------------------------------------------------------
  // Compute errors
  std::cout << FORMAT(50) << "\nComputation of errors begins" << std::endl;  

  Real err_u = 0.;
  Real err_G = 0.;
  Real norm_u_sol = 0.;
  Real norm_G = 0.;        
  

  // for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
  //   const Cell & T = Th->cell(iT);
  //   const LC & lc = Local_Contribs[iT];
  //   auto idx_T = face_unknown_local_idx( Th, iT);

  //   // Reconstruct local discrete solution
  //   const Eigen::VectorXd & u_sol_T = lc.interpolate(Th, iT, sol.u).head(LC::nb_local_cell_dofs);
    
  //   //compute the L2 norm of the exact solution
  //   norm_u_sol += u_sol_T.transpose() * lc.MTT * u_sol_T;

  //   //compute the L2 norm of the gradient of the exact solution
  //   for(Integer iF_loc = 0; iF_loc < T.numberOfFaces(); iF_loc++) {
  //     const PyramidIntegrator & pim = *lc.pims[iF_loc];
  //     for(Integer iQN = 0; iQN < pim.numberOfPoints(); iQN++) {
  //       const Point & xQN = pim.point(iQN);
  //       const Real & wQN = pim.weight(iQN);
  //       const Eigen::Matrix<Real, 2, 1> & G_iqn  = sol.G(xQN);
  //       norm_G += wQN * G_iqn.squaredNorm();
  //     } // for iQN
  //   } // for iF_loc
    
  //   lc.Compute_diff_L2_H1(Th, iT, idx_T, omega, sol.Phi, X, sol.u, sol.G, err_u ,err_G, min_cells, min_faces); 

  // } // for iT

  // norm_u_sol = std::sqrt(norm_u_sol);
  // err_u = std::sqrt(err_u);
  // norm_G = std::sqrt(norm_G);
  // err_G = std::sqrt(err_G);

  // std::cout << FORMAT(50) << "norm_u" << norm_u_sol << std::endl;
  // std::cout << FORMAT(50) << "err_uh" << err_u / norm_u_sol << std::endl;

  // std::cout << FORMAT(50) << "norm_G" << norm_G << std::endl;
  // std::cout << FORMAT(50) << "err_G" << err_G / norm_G << std::endl; 
  std::cout << FORMAT(50) << "\nMin on QN cells" << min_QN_cells << std::endl;  
  std::cout << FORMAT(50) << "\nMin on QN faces" << min_QN_faces << std::endl;  
  std::cout << FORMAT(50) << "\nMin on cells (mean)" << min_cells_mean << std::endl;  
  std::cout << FORMAT(50) << "\nMin on faces (mean)" << min_faces_mean << std::endl;
  std::cout << FORMAT(50) << "\nNumber of negative means on cells" << nb_neg_cells << std::endl;

  std::cout << FORMAT(50) << "\n \n \n" << std::endl;
  std::cout << "expf_"+std::to_string(DEGREE) +" & " << t_tot <<" & "<< N <<" & " << min_cells_mean <<" & " << min_faces_mean <<" & " << min_QN_cells <<" & " << min_QN_faces <<" \\ "<< std::endl;

  //postProcessing_expfitt<ho::HierarchicalScalarBasis2d<DEGREE+1>, boost::static_unsigned_min<DEGREE+1,3>::value>("output/uh_expfitt.vtu", Th, omega, coeffs_rho);
  //postProcessing_poly<ho::HierarchicalScalarBasis2d<DEGREE+1>, boost::static_unsigned_min<DEGREE+1,3>::value>("output/puh.vtu", Th, coeffs_pu);
  
  std::vector<Real> Vec_data;
  Vec_data.push_back(h);
  // Vec_data.push_back( err_u / norm_u_sol);
  // Vec_data.push_back(err_G / norm_G );  
  // Vec_data.push_back( min_cells );
  // Vec_data.push_back( min_faces );
  
  std::cout << "Done" << std::endl;

  return Vec_data;
}


IndexVectorType face_unknown_local_idx(const Mesh * Th,
                                       const Integer & iT)
{
  const Cell & T = Th->cell(iT);

  IndexVectorType idx(T.numberOfFaces() * LC::nb_local_face_dofs);
  std::size_t index_idx = 0;
  for(Integer iF_loc = 0; iF_loc < T.numberOfFaces(); iF_loc++) {
    std::size_t offset_F = T.faceId(iF_loc) * LC::nb_local_face_dofs;

    for(std::size_t i = 0; i < LC::nb_local_face_dofs; i++) {
      idx[index_idx++] = offset_F + i;
    } // for i
  } // for iF_loc

  return idx;
}





///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



/*
int main__()
{ 
    string  geo_mesh ; // = "hexagonal";
    int  i_max ; //= 5;
    int arg; 

    cout << "Mesh geometry ? (name without _, ex : hexagonal or mesh3)" << endl;
    cin >> geo_mesh ;

    cout << "Number of meshes ? " << endl;
    cin >> i_max; 

    std::string Mesh_base = "../meshes/";
    std::vector<vector<Real>> Data_errors;
    Data_errors.resize(i_max);    

    const int iT = 5;
    
    for(int i=0; i< i_max ; i++){
        std::string Mesh_file = Mesh_base + geo_mesh +"_" + std::to_string(i+1) +".typ1";
        const std::shared_ptr<Mesh> Th = Mesh_read(Mesh_file);
        Data_errors[i] = computations_test_loc(Th.get(),iT);

    } //for i 

    std::cout << "Computation of errors for test : OK  \n" << std::endl;
    std::cout << " ~~~~~~~~~~~~~~~~~~~~~ \n" << std::endl;
    std::cout << "Interpolation error on cells / h^(deg +2) : \t" ;
    for(int j = 0; j< i_max; j++){
        std::cout << Data_errors[j][0] << "\t";
    } // for j 

    std::cout << "\n  " << std::endl ;
    std::cout << "Interpolation error on faces / h^(deg +1) : \t" ;
    for(int j = 0; j< i_max; j++){
      std::cout << Data_errors[j][1] << "\t";
    } // for j 

    std::cout << "\n  "<< std::endl ;
    std::cout << "Interpolation error on Gradient / h^(deg +1) :\t " ;
    for(int j = 0; j< i_max; j++){
      std::cout << Data_errors[j][2] << "\t";
    } // for j     
    
    std::cout << "\n  "<< std::endl ;
    std::cout << "Interpolation on faces I_K(u) - u / h^(deg + 1.5) :\t " ;
    for(int j = 0; j< i_max; j++){
      std::cout << Data_errors[j][3] << "\t";
    } // for j   
    
    std::cout << "\n"<< std::endl  ;     
    std::cout << "on faces Pi_K(I_K(u)) - u  / h^(deg +1.5) : \t" ;
    for(int j = 0; j< i_max; j++){
      std::cout << Data_errors[j][4] << "\t";
    } // for j     
    
    std::cout << "\n"<< std::endl  ;     
    std::cout << "on faces Pi_K(I_K(u)) - Pi_F(u)  / h^(deg +1.5) : \t" ;
    for(int j = 0; j< i_max; j++){
      std::cout << Data_errors[j][5] << "\t";
    } // for j 

    std::cout << "\n"<< std::endl  ;     
    std::cout << "stab / h^(deg +1) : \t" ;
    for(int j = 0; j< i_max; j++){
      std::cout << Data_errors[j][6] << "\t";
    } // for j 

    std::cout << "\n ~~~~~~~~~~~~~~~~~~~~~ \n" << std::endl;
    std::cout << "Done" << std::endl;
    return 0;
}
*/




/*
vector<Real> computations_test_loc(const Mesh *Th, const int & iT)
{ 
  
  std::string sep = "\n----------------------------------------\n";
  Eigen::IOFormat CleanFmt(4, 0, ", ", "\n", "[", "]");
  std::cout << "Test of the HHO contributions"  << std::endl;

  std::cout << "Degree: " << DEGREE << std::endl;

  const Real h = Th->meshsize();
  
  const int nb_cells = Th->numberOfCells();
  const int nb_faces = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();



  //------------------------------------------------------------------------------
  // Exact solution

  ho::pho::Regular sol;

  DefBoundaryConditionType isNeu = sol.isNeumann;
  DefBoundaryConditionType isDir = sol.isDirichlet;


  //------------------------------------------------------------------------------
  // Precompute local contributions
  std::cout << "\n Precomputations begin"  << std::endl;  
  common::chrono c_comp;
  c_comp.start();


  const LC  lc(Th, iT, sol.f, sol.g_n,  sol.Lambda, isNeu );

  
  c_comp.stop();
  std::cout << "Precomputations : DONE !"  << std::endl;  
  std::cout << FORMAT(50) << "Time of the precomputational step"  << c_comp.diff() << std::endl;

  Real Er_int_vol = 0.;
  Real Er_int_faces = 0.; 
  Real Er_int_grad = 0.;
  Real Stab_sq = 0.;
  Real Er_int_TF = 0.; 
  Real Er_int_ProjT_F = 0.;
  Real Er_ProjT_ProjF = 0.; 
  Real Er_jumps_I = 0.; 

  const Cell & T = Th->cell(iT);
  IndexVectorType idx_T = face_unknown_local_idx( Th, iT);
  const Real & h_T = cell_diameter(Th, iT) ;
    
  const std::size_t NG_full = lc.NG_full;
  const std::size_t N_basis_T = lc.nb_local_cell_dofs;

  //extraction matrix
  Eigen::MatrixXd Extract_T = Eigen::MatrixXd::Zero(lc.nb_cell_dofs, lc.nb_tot_dofs); // Matrix for the extraction of the cell dofs
  for(int i=0; i< lc.nb_cell_dofs; i++){
    Extract_T(i,i) += 1.;      
  } //for i

  // computation of the interpolation of u 
  const Eigen::VectorXd & u_TF = lc.interpolate(Th, iT, sol.u); 
  const Eigen::VectorXd & u_T = u_TF.head(LC::nb_local_cell_dofs);

  std::cout << "extract on T ?? \n" <<  u_T - Extract_T * u_TF << std::endl; 
    
  Eigen::VectorXd G_T_coeffs = lc.G_full_T * u_TF; // computation of the coefficients of the discrete (full) gradient in the vectorial polynomial basis !

  for(Integer iF_loc = 0; iF_loc < T.numberOfFaces(); iF_loc++) {
    const int iF = T.faceId(iF_loc);    
    const int offset_F_loc = lc.nb_cell_dofs + iF_loc * LC::FaceBasis::size;
    const Eigen::VectorXd & u_F = u_TF.segment(offset_F_loc, LC::FaceBasis::size );     
    
    Eigen::MatrixXd Extract_F = Eigen::MatrixXd::Zero(lc.nb_local_face_dofs, lc.nb_tot_dofs); //extraction matrices for the coordinates of the faces
    for(int i = 0; i < lc.nb_local_face_dofs; i++){
      Extract_F(i , offset_F_loc + i) += 1.; 
    } // for i 
    
    std::cout << "extract on F ?? \n" <<  u_F - Extract_F * u_TF << std::endl;       

    const PyramidIntegrator & pim = *lc.pims[iF_loc];
    BasisFunctionEvaluation<LC::CellVecBasis> feval_vec_basisT_PTF(lc.basis_vec_T.get(), pim.points());
    BasisFunctionEvaluation<LC::CellBasis> feval_basisT_PTF(lc.basisT.get(), pim.points());

    for(Integer iQN = 0; iQN < pim.numberOfPoints(); iQN++) {
      const Point & xQN = pim.point(iQN);
      const Real & wQN = pim.weight(iQN);        
        
      //computation of the interpolation error 
      const Real u_ex_iqn = sol.u(xQN); 
      Real u_iqn = 0. ; 
      for(std::size_t i = 0; i < N_basis_T ; i++) {
        const LC::CellBasis::ValueType & phi_i_iqn = feval_basisT_PTF(i, iQN);
        u_iqn += u_T(i) * phi_i_iqn;
      } // for i
      Er_int_vol += wQN * (u_ex_iqn - u_iqn) * (u_ex_iqn - u_iqn); 
        
      //computation of the "interpolation" error on gradient 
      const Eigen::Matrix<Real, 2, 1> & G_ex_iqn  = sol.G(xQN);
      Eigen::Matrix<Real, 2, 1> G_iqn = Eigen::Matrix<Real, 2, 1>::Zero();
      for(std::size_t i = 0; i < NG_full; i++) {
        const LC::CellVecBasis::ValueType & Tau_i_iqn = feval_vec_basisT_PTF(i, iQN);
        G_iqn += G_T_coeffs(i) * Tau_i_iqn;
      } // for i
      Er_int_grad += wQN * (G_ex_iqn - G_iqn).dot(G_ex_iqn - G_iqn);
    } // for iQN


    Real Er_face =0.;
    Real Er_TF =0.;
    Real Er_jump =0.;  
    Real Er_proj =0.;  
    Real Er_projTF =0.;  
      
    const FaceIntegrator & fim = *lc.fims[iF_loc];
    BasisFunctionEvaluation<LC::FaceBasis> feval_basisF(lc.basisF[iF_loc].get(), fim.points());  
    BasisFunctionEvaluation<LC::CellBasis> feval_basisT_F(lc.basisT.get(), fim.points());       
    for(Integer iQN = 0; iQN < fim.numberOfPoints(); iQN++) {
      const Point & xQN = fim.point(iQN);
      const Real & wQN = fim.weight(iQN);        
        
      //computation of the interpolation error on the face F
      const Real u_ex_iqn = sol.u(xQN); 
      Real u_F_iqn = 0. ; 
      Real u_T_F_iqn = 0. ; 
      Real proj_u_T_F_iqn = 0. ; 
      
      const Eigen::VectorXd & proj_u_T_F = lc.LU_MFF[iF_loc].solve(lc.MFT[iF_loc] )* u_T;
      for(std::size_t i = 0; i < LC::FaceBasis::size ; i++) {
        const LC::FaceBasis::ValueType & phi_F_i_iqn = feval_basisF(i, iQN);
        u_F_iqn += u_F(i) * phi_F_i_iqn;
        proj_u_T_F_iqn += proj_u_T_F(i) * phi_F_i_iqn;
      } // for i
      Er_face += wQN * (u_ex_iqn - u_F_iqn) * (u_ex_iqn - u_F_iqn);  

      
             
      for(std::size_t i = 0; i < LC::CellBasis::size ; i++) {
        const LC::FaceBasis::ValueType & phi_T_i_iqn = feval_basisT_F(i, iQN);
        u_T_F_iqn += u_T(i) * phi_T_i_iqn;
      } // for i
      Er_TF += wQN * (u_T_F_iqn - u_ex_iqn) * (u_T_F_iqn - u_ex_iqn);
      Er_proj += wQN * (proj_u_T_F_iqn - u_ex_iqn) * (proj_u_T_F_iqn - u_ex_iqn) ;
      Er_projTF += wQN * (proj_u_T_F_iqn - u_F_iqn) * (proj_u_T_F_iqn - u_F_iqn) ;
    } // for iQN
    Er_int_faces += Er_face;
    Er_int_TF += Er_TF;
    Er_int_ProjT_F += Er_proj;  
    Er_ProjT_ProjF += Er_projTF; 
  } // for iF_loc

  //stabilisation term 
  Stab_sq = u_TF.transpose() * lc.STF * u_TF; // stabilisation S(u,u)


  std::vector<Real> Vec_data;
  Vec_data.push_back( sqrt(Er_int_vol) / std::pow(h_T,DEGREE + 2));
  Vec_data.push_back( sqrt(Er_int_faces) / std::pow(h_T,DEGREE + 1));
  Vec_data.push_back( sqrt(Er_int_grad) / std::pow(h_T,DEGREE + 1)); 
  Vec_data.push_back( sqrt(Er_int_TF) / std::pow(h_T,DEGREE + 1.5));
  Vec_data.push_back( sqrt(Er_int_ProjT_F) / std::pow(h_T,DEGREE + 1.5));  
  Vec_data.push_back( sqrt(Er_ProjT_ProjF) / std::pow(h_T,DEGREE + 1.5));
  Vec_data.push_back( sqrt(Stab_sq) / std::pow(h_T,DEGREE +1));

  return Vec_data;
}

*/

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


