#ifndef GAUSS_HPP
#define GAUSS_HPP

#include "Eigen/Dense"

#include "qr.hpp"

namespace fe {

  inline bool isOdd(int N) { return (N % 2); }
  inline bool isEven(int N) { return not(isOdd(N)); }

  /*!
    \class Gauss
    \author Daniele A. Di Pietro
    \date 2006-11-27
    \brief Return Gauss' quadrature on the reference segment given the
    degree of exactness d
  */

  template<typename T>
  class Gauss : public QuadratureRule<T> {
  public:
    /** @name Typedefs
     */
    //@{
    //! Scalar type
    typedef typename QuadratureRule<T>::real_type real_type;
    //! Node type
    typedef typename QuadratureRule<T>::node_type node_type;
    //@}

    /** @name Constructors
     */
    //@{
    //! Construct the rule given the degree of exactness
    Gauss(const int & a_d);
    //@}

    /** @name Methods
     */
    //@{
    static inline int d2N(int a_d)
    {
      int d = a_d;
      if( isEven(d) ) d++;
      return std::max( (d+1)/2, 1);
    }
    //@}
  };

  ////////////////////////////////////////////////////////////
  // Implementation

  template<typename T>
  Gauss<T>::Gauss(const int & a_d) :
  QuadratureRule<T>(d2N(a_d), a_d)
  {
    // Only odd degrees of exactness can be obtained
    int d = a_d;
    if( isEven(d) ) d++;
    // Compute one-dimensional nodes and weights
    if(this->numberOfNodes() == 1) {
      this->weight()[0] = 1.;
      this->nodes()[0] = node_type(0.5, 0., 0.);
    } else {
      Eigen::MatrixXd M = Eigen::MatrixXd::Zero(this->numberOfNodes(), this->numberOfNodes());
      for(int i = 0; i < this->numberOfNodes() - 1; i++) {
        M(i+1,i) = std::sqrt( 1. / ( 4. - 1. / std::pow(i + 1., 2) ) );
      } // for i
      Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> es;
      es.compute(M);
      Eigen::VectorXd weights = es.eigenvectors().row(0);
      Eigen::VectorXd nodes = es.eigenvalues();
      for(int i = 0; i < this->numberOfNodes(); i++) {
        this->weight()[i] = pow(weights(i), 2);
        this->nodes()[i] = (nodes(i) + 1.) / 2.;
      } // for i
    } // else
  }

} // namespace fe

#endif
