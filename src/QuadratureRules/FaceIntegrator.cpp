#include "FaceIntegrator.h"

#include "gauss.hpp"

FaceIntegrator::FaceIntegrator(const Mesh * Th, const Integer & iF, const Integer & doe)
{
  VF_ASSERT(iF < Th->numberOfFaces());
  const Face & F = Th->face(iF);
  const Real & mF = F.measure();
  Point P0 = F.point(0).first;
  Point P1 = F.point(1).first;

  Integer _doe = std::max(doe, 1);
  fe::Gauss<Real> qr(_doe);

  m_points.resize(qr.numberOfNodes());
  m_weights.resize(qr.numberOfNodes());

  // Map onto the physical face
  for(Integer iQN = 0; iQN < qr.numberOfNodes(); iQN++) {
    m_weights[iQN] = qr.weight(iQN) * mF;
    m_points[iQN] = P0 + qr.node(iQN)(0) * (P1 - P0);
  }
}
