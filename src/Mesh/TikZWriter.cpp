#include "TikZWriter.h"

#include <fstream>
#include <iomanip>

void TikZWriter(const Mesh & Th, const std::string & file_name, bool plot_centers)  
{
  // Open file
  std::ofstream file(file_name, std::ios::out);
  // Change the way numbers are represented
  file.precision(8);
  file.flags(std::ios::fixed);
  // Begin tikz environment
  file << "\\begin{tikzpicture}\n";
  // Loop over cells
  for(Integer iK = 0; iK < Th.numberOfCells(); iK++) {
    const auto & K = Th.cell(iK);
    // Draw cell contour
    file << "  \\draw plot coordinates {";
    auto P0 = Th.node(K.nodeId(0)).point();
    for(Integer iP = 0; iP < K.numberOfNodes(); iP++) {
      const auto & P = Th.node(K.nodeId(iP)).point();
      file << "(" << P(0) << "," << P(1) << ") ";
    }
    file << "(" << P0(0) << "," << P0(1) << ")};\n";
    // Draw cell center if required
    if(plot_centers) {
      const auto & xK = K.center();
      file << "  \\fill (" << xK(0) << "," << xK(1)
           << ") circle (0.02);\n";
    }
  }
  // End tikz environment
  file << "\\end{tikzpicture}\n";
}
