#include "vectorProduct.h"

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

template<>
boost::numeric::ublas::bounded_vector<Real, 2> 
vector_product<2>(const boost::numeric::ublas::bounded_vector<Real, 2> & a_x, 
                  const boost::numeric::ublas::bounded_vector<Real, 2> & a_y)
{
  boost::numeric::ublas::bounded_vector<Real, 2> result;

  result(0) = a_x(0)*a_y(1)-a_y(0)*a_x(1);
  result(1) = 0;

  return result;
}

/*----------------------------------------------------------------------------*/

template<>
boost::numeric::ublas::bounded_vector<Real, 3> 
vector_product<3>(const boost::numeric::ublas::bounded_vector<Real, 3> & a_x, 
                  const boost::numeric::ublas::bounded_vector<Real, 3> & a_y)
{
  boost::numeric::ublas::bounded_vector<Real, 3> result;

  result(0) = a_x(1)*a_y(2)-a_y(1)*a_x(2);
  result(1) = a_x(2)*a_y(0)-a_y(2)*a_x(0);
  result(2) = a_x(0)*a_y(1)-a_y(0)*a_x(1);

  return result;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
