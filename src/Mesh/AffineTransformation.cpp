#include <boost/numeric/ublas/io.hpp>
#include "AffineTransformation.h"

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

Point AffineTransformation::apply(const Point & a_P)
{
  Point Q;  
  Q(0)=m_alpha_x.first*a_P(0)+m_alpha_x.second;
  Q(1)=m_alpha_y.first*a_P(1)+m_alpha_y.second;
  return Q;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
