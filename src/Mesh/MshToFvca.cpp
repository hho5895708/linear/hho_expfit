#include "MshToFvca.h"

#include <fstream>
#include <iomanip>
#include <tuple>

void MshToFvca(std::string & msh_file_name)  
{

  // Open source file (.msh)
  std::fstream msh_file(msh_file_name.c_str(), std::ios::in);
  std::string marker;

  // Open target file (.typ1)
  std::size_t idx = msh_file_name.find('.');
  std::string fvca_file_name = msh_file_name.substr(0,idx);
  fvca_file_name += ".typ1";
  std::ofstream fvca_file(fvca_file_name, std::ios::out);

  // Change the way numbers are represented
  fvca_file.precision(10);
  fvca_file.flags(std::ios::fixed);

  // Read and write nodes
  fvca_file << "vertices\n";
  msh_file >> marker;
  while(marker.compare("$Nodes")) {
    msh_file >> marker;
    assert(!msh_file.eof());
  }
  Integer N_nodes;
  msh_file >> N_nodes;
  fvca_file << "        " << N_nodes << "\n"; 
  std::cout << "Reading and writing " << N_nodes << " nodes" << std::endl;
  for(Integer i = 0; i < N_nodes; i++) {
    Real x, y;
    msh_file >> marker;
    msh_file >> x >> y;
    fvca_file << "   " << x << "   " << y << "\n";
    msh_file >> marker;
  }

  // Read elements
  msh_file >> marker;
  while(marker.compare("$Elements")) {
    msh_file >> marker;
    assert(!msh_file.eof());
  }
  Integer N_elements;
  msh_file >> N_elements;
  std::cout << "Reading " << N_elements << " elements" << std::endl;
  Integer N_triangles = 0;
  Integer N_bnd_faces = 0;
  Integer N_faces = 0;
  std::vector<std::tuple<Integer,Integer,Integer> > triangles;
  std::vector<std::pair<Integer,Integer> > bnd_faces;
  std::vector<std::tuple<Integer,Integer,Integer,Integer> > faces;
  Integer N_type;
  msh_file >> marker >> N_type;
  while(N_type == 1) {
    Integer N1, N2;
    msh_file >> marker >> marker >> marker >> N1 >> N2;
    bnd_faces.resize(N_bnd_faces+1);
    bnd_faces[N_bnd_faces] = std::make_pair(N1,N2);
    faces.resize(N_faces+1);
    faces[N_faces] = std::make_tuple(N1,N2,0,0);
    ++N_bnd_faces;
    ++N_faces;
    msh_file >> marker >> N_type;
  }
  while(N_type == 2) {
    Integer N1, N2, N3;
    msh_file >> marker >> marker >> marker >> N1 >> N2 >> N3;
    triangles.resize(N_triangles+1);
    triangles[N_triangles] = std::make_tuple(N1,N2,N3);
    ++N_triangles;
    // Test [N1,N2]
    bool test = true;
    bool test_2 = true;
    for(Integer i = 0; i < N_bnd_faces; i++) {
      if((bnd_faces[i].first==N1 && bnd_faces[i].second==N2) || (bnd_faces[i].second==N1 && bnd_faces[i].first==N2)) {
	std::get<2>(faces[i]) = N_triangles;
	test = false;
	break;
      }
    }
    if (test) {
      for(Integer i = N_bnd_faces; i < N_faces; i++) {
	if((std::get<0>(faces[i])==N1 && std::get<1>(faces[i])==N2) || (std::get<1>(faces[i])==N1 && std::get<0>(faces[i])==N2)) {
	  std::get<3>(faces[i]) = N_triangles;
	  test_2 = false;
	  break;
	}
      }
      if (test_2) {
	faces.resize(N_faces+1);
	faces[N_faces] = std::make_tuple(N1,N2,N_triangles,0);
	++N_faces;
      }
    }
    // Test [N1,N3]
    test = true;
    test_2 = true;
    for(Integer i = 0; i < N_bnd_faces; i++) {
      if((bnd_faces[i].first==N1 && bnd_faces[i].second==N3) || (bnd_faces[i].second==N1 && bnd_faces[i].first==N3)) {
	std::get<2>(faces[i]) = N_triangles;
	test = false;
	break;
      }
    }
    if (test) {
      for(Integer i = N_bnd_faces; i < N_faces; i++) {
	if((std::get<0>(faces[i])==N1 && std::get<1>(faces[i])==N3) || (std::get<1>(faces[i])==N1 && std::get<0>(faces[i])==N3)) {
	  std::get<3>(faces[i]) = N_triangles;
	  test_2 = false;
	  break;
	}
      }
      if (test_2) {
	faces.resize(N_faces+1);
	faces[N_faces] = std::make_tuple(N1,N3,N_triangles,0);
	++N_faces;
      }
    }
    // Test [N2,N3]
    test = true;
    test_2 = true;
    for(Integer i = 0; i < N_bnd_faces; i++) {
      if((bnd_faces[i].first==N2 && bnd_faces[i].second==N3) || (bnd_faces[i].second==N2 && bnd_faces[i].first==N3)) {
	std::get<2>(faces[i]) = N_triangles;
	test = false;
	break;
      }
    }
    if (test) {
      for(Integer i = N_bnd_faces; i < N_faces; i++) {
	if((std::get<0>(faces[i])==N2 && std::get<1>(faces[i])==N3) || (std::get<1>(faces[i])==N2 && std::get<0>(faces[i])==N3)) {
	  std::get<3>(faces[i]) = N_triangles;
	  test_2 = false;
	  break;
	}
      }
      if (test_2) {
	faces.resize(N_faces+1);
	faces[N_faces] = std::make_tuple(N2,N3,N_triangles,0);
	++N_faces;
      }
    }
    msh_file >> marker;
    if(!marker.compare("$EndElements"))
      break;
    else
      msh_file >> N_type;
  }

  // Write triangles, edges of the boundary, and all edges

  fvca_file << "triangles\n";
  fvca_file << "        " << N_triangles << "\n"; 
  std::cout << "Writing " << N_triangles << " triangles" << std::endl;
  for(Integer i = 0; i < N_triangles; i++)
    fvca_file << "     " << std::get<0>(triangles[i]) << "     " << std::get<1>(triangles[i]) << "     " << std::get<2>(triangles[i]) << "\n";
  std::cout << "Writing " << 0 << " quad, pent, hex" << std::endl;
  fvca_file << "quadrangles\n";
  fvca_file << "        " << 0 << "\n";
  fvca_file << "pentagons\n";
  fvca_file << "        " << 0 << "\n";
  fvca_file << "hexagons\n";
  fvca_file << "        " << 0 << "\n";
  std::cout << "Writing " << N_bnd_faces << " boundary edges" << std::endl;
  fvca_file << "edges of the boundary\n";
  fvca_file << "        " << N_bnd_faces << "\n";
  for(Integer i = 0; i < N_bnd_faces; i++)
    fvca_file << "     " << bnd_faces[i].first << "     " << bnd_faces[i].second << "\n";
  std::cout << "Writing " << N_faces << " edges" << std::endl;
  fvca_file << "all edges\n";
  fvca_file << "        " << N_faces << "\n";
  for(Integer i = 0; i < N_faces; i++)
    fvca_file << "     " << std::get<0>(faces[i]) << "     " << std::get<1>(faces[i]) << "     " << std::get<2>(faces[i]) << "     " << std::get<3>(faces[i]) << "\n";

  std::cout << "Done" << std::endl;

}
